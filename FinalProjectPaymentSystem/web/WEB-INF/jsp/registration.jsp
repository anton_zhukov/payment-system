<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/WEB-INF/include/locale.jsp" %>
<html>
<head>
    <title><fmt:message key="title.registration"/></title>
    <%@ include file="/WEB-INF/include/js.jsp" %>
</head>

<body>
<div class="content">
    <%@ include file="/WEB-INF/include/header.jsp" %>

    <div class="main">
        <%@ include file="/WEB-INF/include/errors.jsp" %>
        <div class="login-registration-block">
            <span>
                <b><fmt:message key="message.registration.header"/></b>
                <button onclick="location.href = '/do?action=viewLogin'" id="back-to-login">
                    <fmt:message key="registration.button.back-to-login"/>
                </button>
            </span>

            <form action="/do?action=registrateUser" method="post" onsubmit="return validateRegistrationForm(this);">
                <div>
                    <span class="loginEmpty error" style="display: none;">
                        <fmt:message key="common.js-validation.fill-this-field"/>
                    </span>
                    <input type="text" value="${requestScope.user.login}" placeholder=
                            "<fmt:message key="registration.input.user-name"/>" name="login" required="required"/>
                    <span class="star">*</span>
                </div>

                <div>
                    <span class="passwordError error" style="display: none;">
                        <fmt:message key="registration.js-validation.wrong-pass-length"/>
                    </span>
                    <span class="passwordEmpty error" style="display: none;">
                        <fmt:message key="common.js-validation.fill-this-field"/>
                    </span>
                    <input type="password" placeholder=
                            "<fmt:message key="registration.input.password"/>" name="password" required="required"/>
                    <span class="star">*</span>
                </div>

                <div>
                    <span class="repeatPasswordError error" style="display: none;">
                        <fmt:message key="registration.js-validation.repeat-pass-wrong"/>
                    </span>
                    <span class="repeatPasswordEmpty error" style="display: none;">
                        <fmt:message key="common.js-validation.fill-this-field"/>
                    </span>
                    <input type="password" placeholder="<fmt:message key="registration.input.repeat-password"/>"
                           name="repeat_password" required="required"/>
                    <span class="star">*</span>
                </div>

                <div>
                    <span class="nameError error" style="display: none;">
                        <fmt:message key="registration.js-validation.wrong-name-form"/>
                    </span>
                    <span class="nameEmpty error" style="display: none;">
                        <fmt:message key="common.js-validation.fill-this-field"/>
                    </span>

                    <input type="text" value="${requestScope.user.name}"
                           placeholder="<fmt:message key="registration.input.name"/>" name="name" required="required"/>
                    <span class="star">*</span>
                </div>

                <div>
                <span class="surnameError error" style="display: none;">
                    <fmt:message key="registration.js-validation.wrong-surname-form"/>
                </span>
                <span class="surnameEmpty error" style="display: none;">
                    <fmt:message key="common.js-validation.fill-this-field"/>
                </span>

                    <input type="text" value="${requestScope.user.surname}" placeholder=
                            "<fmt:message key="registration.input.surname"/>" name="surname" required="required"/>
                    <span class="star">*</span>
                </div>

                <div>
                <span class="emailError error" style="display: none;">
                    <fmt:message key="registration.js-validation.wrong-email-form"/>
                </span>
                <span class="emailEmpty error" style="display: none;">
                    <fmt:message key="common.js-validation.fill-this-field"/>
                </span>

                    <input type="text" value="${requestScope.user.emailAddress}" placeholder=
                            "<fmt:message key="registration.input.e-mail"/>" name="email" required="required"/>
                    <span class="star">*</span>
                </div>

                <div>
                    <fmt:message key="registration.button.registrate" var="buttonValueSignUp"/>
                    <button type="submit">${buttonValueSignUp}</button>
                </div>
            </form>
        </div>
    </div>
    <div class="footer-push"></div>
</div>

<c:import url="/WEB-INF/include/footer.jsp"/>
</body>
</html>
