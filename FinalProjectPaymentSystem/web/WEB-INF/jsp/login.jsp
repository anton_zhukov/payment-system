<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/include/locale.jsp" %>

<html>
<head>
    <title><fmt:message key="title.login"/></title>
    <%@ include file="/WEB-INF/include/js.jsp" %>
</head>
<body>

<div class="content">
    <%@ include file="/WEB-INF/include/header.jsp" %>
    <div class="main">
        <c:choose>
            <c:when test="${not empty sessionScope.user}">
                <c:choose>
                    <c:when test="${sessionScope.user.type==1}">
                        <c:redirect url="/do?action=viewAdmin"/>
                    </c:when>
                    <c:otherwise>
                        <c:redirect url="/do?action=listAccounts"/>
                    </c:otherwise>
                </c:choose>
            </c:when>

            <c:otherwise>
                <div class="login-registration-block">
                    <div id="login-error-messages">
                        <%@ include file="/WEB-INF/include/errors.jsp" %>
                    </div>
                    <form method="post" action="/do?action=userLogin" onsubmit="return validateLoginForm(this);">
                        <div>
                            <input type="text" value="${requestScope.login}" placeholder=
                                    "<fmt:message key="login.input.login"/>" name="login" required="required">
                            <span class="loginEmpty" style="display: none">
                                <fmt:message key="common.js-validation.fill-this-field"/>
                            </span>
                        </div>
                        <div>
                            <input type="password" placeholder=
                                    "<fmt:message key="login.input.password"/>" name="password" required="required">
                            <span class="passwordEmpty" style="display: none">
                                <fmt:message key="common.js-validation.fill-this-field"/>
                            </span>
                        </div>
                        <div>
                            <fmt:message key="login.button.sign-in" var="buttonValueLogIn"/>
                            <button type="submit">${buttonValueLogIn}</button>
                        </div>
                    </form>

                    <div class="sign-up">
                        <a href="/do?action=viewRegistration" class="icon-holder add_user">
                            <fmt:message key="login.reference.add-user"/>
                        </a>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
    <div class="footer-push"></div>
</div>

<c:import url="/WEB-INF/include/footer.jsp"/>
</body>
</html>