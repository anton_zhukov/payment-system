<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/include/locale.jsp" %>
<html>

<head>
    <title>
        <fmt:message key="title.post-registration"/>
    </title>
    <%@ include file="/WEB-INF/include/js.jsp" %>
</head>

<body>

<div class="content">
    <%@ include file="/WEB-INF/include/header.jsp" %>
    <div class="main">
        <a href="/do?action=viewLogin">
            <fmt:message key="post-registration.reference.back-and-log-in"/>
        </a>
    </div>
    <div class="footer-push"></div>
</div>

<c:import url="/WEB-INF/include/footer.jsp"/>
</body>

</html>
