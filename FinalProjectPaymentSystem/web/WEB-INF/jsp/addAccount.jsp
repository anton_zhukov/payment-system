<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="/WEB-INF/include/locale.jsp" %>
<html lang="${language}">
<head>
    <title><fmt:message key="title.add-account"/></title>
    <%@ include file="/WEB-INF/include/js.jsp" %>
</head>

<body>
<div class="content">
    <%@ include file="/WEB-INF/include/header.jsp" %>

    <div class="main">
        <%@ include file="/WEB-INF/include/errors.jsp" %>
        <form action="/do?action=addAccount" method="post" onsubmit="return validateAddAccountForm(this);">
            <div>
                <input type="text" placeholder="<fmt:message key="add-account.input.init-balance"/>" name="balance"
                       value="${requestScope.account.currentBalance}" required="required"/>
                <span class="initBalanceFormatError error" style="display: none;">
                    <fmt:message key="add-account.js-validation.wrong-init-balance-format"/>
                </span>
                <span class="initBalanceMaxRangeError error" style="display: none;">
                    <fmt:message key="add-account.js-validation.wrong-init-balance-max-range"/>
                </span>
            </div>
            <div>
                <select name="bank" class="select top">
                    <c:forEach var="bank" items="${banks}">
                        <option value="${bank.bankId}" <c:if test="${account.bankId eq bank.bankId}">selected</c:if>>
                                ${bank.bankName}
                        </option>
                    </c:forEach>
                </select>
            </div>
            <div>
                <select name="currency" class="select top">
                    <c:forEach var="curr" items="${currencies}">
                        <option value="${curr.currencyId}"
                                <c:if test="${account.currencyId eq curr.currencyId}">selected</c:if>>
                                ${curr.currencyCode}
                        </option>
                    </c:forEach>
                </select>
            </div>
            <div>
                <fmt:message key="add-account.button.add-account" var="buttonAddAccount"/>
                <button type="submit" class="submit-button top">${buttonAddAccount}</button>
            </div>
        </form>
    </div>
    <div class="footer-push"></div>
</div>

<c:import url="/WEB-INF/include/footer.jsp"/>
</body>
</html>
