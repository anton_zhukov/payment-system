<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="/WEB-INF/include/locale.jsp" %>
<html>
<head>
    <title><fmt:message key="title.add-currency"/></title>
    <%@ include file="/WEB-INF/include/js.jsp" %>
</head>
<body>
<div class="content">
    <%@ include file="/WEB-INF/include/header.jsp" %>

    <div class="main">
        <%@ include file="/WEB-INF/include/errors.jsp" %>
        <div>
            <a href="/do?action=listCurrencies" class="icon-holder back">
                <fmt:message key="common.reference.back"/>
            </a>
        </div>
        <form action="/do?action=addCurrency" method="post" onsubmit="return validateAddCurrencyForm(this)">
            <div>
                <input type="text" placeholder="<fmt:message key="add-currency.input.currency-code"/>"
                       name="currencyCode" class="top" required="required" value="${requestScope.currency.currencyCode}"/>
                <span class="currencyCodeError error" style="display: none">
                    <fmt:message key="add-currency.js-validation.wrong-currency-code-format"/>
                </span>
            </div>
            <div>
                <input type="text" placeholder="<fmt:message key="add-currency.input.min-amount"/>" name="minAmount"
                       class="top" required="required" value="${requestScope.currency.minAmount}"/>
                <span class="currencyMinAmountError error" style="display: none">
                    <fmt:message key="add-currency.js-validation.wrong-amount-min-range"/>
                </span>
                <span class="currencyMaxAmountError error" style="display: none">
                    <fmt:message key="add-currency.js-validation.wrong-amount-max-range"/>
                </span>
                <span class="currencyAmountFormatError error" style="display: none">
                    <fmt:message key="add-currency.js-validation.wrong-amount-format"/>
                </span>
            </div>
            <div>
                <fmt:message key="add-currency.button.add" var="buttonAddCurrency"/>
                <button type="submit" class="submit-button top">${buttonAddCurrency}</button>
            </div>
        </form>
    </div>
    <div class="footer-push"></div>
</div>
<c:import url="/WEB-INF/include/footer.jsp"/>
</body>
</html>