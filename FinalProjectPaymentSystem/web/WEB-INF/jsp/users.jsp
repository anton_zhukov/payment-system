<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/include/locale.jsp" %>
<html>
<head>
    <title><fmt:message key="title.users"/></title>
    <%@ include file="/WEB-INF/include/js.jsp" %>
</head>

<body>
<div class="content">
    <%@ include file="/WEB-INF/include/header.jsp" %>

    <div class="main">
        <table id="users-table">
            <tr>
                <th>
                    <b><fmt:message key="users.column-header.login"/></b>
                </th>
                <th>
                    <b><fmt:message key="users.column-header.name"/></b>
                </th>
                <th>
                    <b><fmt:message key="users.column-header.surname"/></b>
                </th>
                <th>
                    <b><fmt:message key="users.column-header.e-mail"/></b>
                </th>
                <th>
                    <b><fmt:message key="users.column-header.not-active-accounts"/></b>
                </th>
            </tr>

            <c:forEach var="userDetails" items="${users}">
                <tr>
                    <td>
                        <a href="/do?action=listAccounts&userId=${userDetails.user.userId}"
                           title="<fmt:message key="users.title.go-to-accounts"/>">
                                ${userDetails.user.login}
                        </a>
                    </td>
                    <td>
                        <c:out value="${userDetails.user.name}"/>
                    </td>
                    <td>
                        <c:out value="${userDetails.user.surname}"/>
                    </td>
                    <td>
                        <c:out value="${userDetails.user.emailAddress}"/>
                    </td>
                    <td>
                        <c:out value="${userDetails.notActiveAccounts}"/>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
    <div class="footer-push"></div>
</div>

<c:import url="/WEB-INF/include/footer.jsp"/>
</body>
</html>