<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="/WEB-INF/include/locale.jsp" %>
<html lang="${language}">
<head>
    <title><fmt:message key="title.replenish-balance"/></title>
    <%@ include file="/WEB-INF/include/js.jsp" %>
</head>

<body>

<div class="content">
    <%@ include file="/WEB-INF/include/header.jsp" %>
    <div class="main">
        <%@ include file="/WEB-INF/include/errors.jsp" %>
        <div>
            <a href="/do?action=listCards&accountId=${param.accountId}" class="icon-holder back">
                <fmt:message key="common.reference.back"/>
            </a>
        </div>

        <form action="/do?action=replenishBalance&accountId=${param.accountId}" method="post"
              onsubmit="return validateReplenishBalanceForm(this);">

            <div class="top">
                <input type="text" placeholder="<fmt:message key="replenish-balance.input.amount"/>"
                       name="replenishAmount" required="required" value="${requestScope.replenishAmount}"/>
                <span class="replenishError error" style="display: none;">
                    <fmt:message key="replenish-balance.js-validation.amount"/>
                </span>
                <span class="replenishAmountMinRangeError error" style="display: none;">
                    <fmt:message key="replenish-balance.js-validation.amount-min-range"/>
                </span>
                <span class="replenishAmountMaxRangeError error" style="display: none;">
                    <fmt:message key="replenish-balance.js-validation.amount-max-range"/>
                </span>
            </div>

            <input type="hidden" name="accountId" value="${param.accountId}"/>

            <button type="submit" class="submit-button top">
                <fmt:message key="replenish-balance.reference.replenish-balance"/>
            </button>
        </form>
    </div>
    <div class="footer-push"></div>
</div>

<c:import url="/WEB-INF/include/footer.jsp"/>
</body>
</html>
