<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/include/locale.jsp" %>
<html lang="${language}">
<head>
    <title><fmt:message key="title.admin"/></title>
    <%@ include file="/WEB-INF/include/js.jsp" %>
</head>
<body>
<div class="content">
    <%@ include file="/WEB-INF/include/header.jsp" %>

    <div class="main">
        <div class="admin-container">
            <div class="admin-item">
                <a href="/do?action=listUsers"><img src="/images/users.png" width="100" height="100" alt="Watch users">
                <span style="display: block">
                    <fmt:message key="admin.reference.users"/>
                </span>
                </a>
            </div>
            <div class="admin-item">
                <a href="/do?action=listBanks"><img src="/images/bank.png" width="100" height="100" alt="Watch banks">
                <span style="display: block">
                    <fmt:message key="admin.reference.banks"/>
                </span>
                </a>
            </div>
            <div class="admin-item">
                <a href="/do?action=listCurrencies"><img src="/images/currencies.png" width="100" height="100"
                                                         alt="Watch currencies">
                <span style="display: block">
                    <fmt:message key="admin.reference.currencies"/>
                </span>
                </a>
            </div>
        </div>
    </div>
    <div class="footer-push"></div>
</div>

<c:import url="/WEB-INF/include/footer.jsp"/>

</body>
</html>