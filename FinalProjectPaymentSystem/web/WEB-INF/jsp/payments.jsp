<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/include/locale.jsp" %>
<html>
<head>
    <title><fmt:message key="title.payments"/></title>
    <%@ include file="/WEB-INF/include/js.jsp" %>
</head>
<body>
<div class="content">
    <%@ include file="/WEB-INF/include/header.jsp" %>
    <div class="main">
        <div id="back-to-cards-from-payments">
            <a href="/do?action=listCards&accountId=${param.accountId}" class="icon-holder back">
                <fmt:message key="common.reference.back"/>
            </a>
            <c:if test="${empty payments}">
                <div class="top">
                    <fmt:message key="payments.message.no-payments"/>
                </div>
            </c:if>
        </div>
        <table id="payments-table">
            <tr>
                <th>
                    <b><fmt:message key="payments.column-header.payment-amount"/></b>
                </th>
                <th>
                    <b><fmt:message key="payments.column-header.card-number"/></b>
                </th>
                <th>
                    <b><fmt:message key="payments.column-header.receiver-account"/></b>
                </th>
                <th>
                    <b><fmt:message key="payments.column-header.payment-date"/></b>
                </th>
            </tr>

            <c:forEach var="paymentDetails" items="${payments}">
                <tr>
                    <td>
                        <fmt:formatNumber type="number" maxFractionDigits="2"
                                          value="${paymentDetails.payment.amount}"/>
                            ${paymentDetails.currency.currencyCode}
                    </td>
                    <td>
                        <fmt:formatNumber type="number" pattern="####,####,####,####"
                                          value="${paymentDetails.creditCard.cardNumber}"/>
                    </td>
                    <td>
                            ${paymentDetails.payment.receiverAccount}
                    </td>
                    <td>
                        <fmt:formatDate value="${paymentDetails.payment.paymentDate}" pattern="dd/MM/yyyy HH:mm:ss"/>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
    <div class="footer-push"></div>
</div>

<c:import url="/WEB-INF/include/footer.jsp"/>
</body>
</html>