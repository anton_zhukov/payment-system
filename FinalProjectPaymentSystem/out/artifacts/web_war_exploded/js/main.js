$(function () {
    var $form = $('#changeLocaleForm');
    var $select = $form.find('select');
    $select.change(function () {
        $form.submit();
    });
});

function validateLoginForm(form) {
    var $form = $(form);
    var login = $form.find('input[name=login]').val();
    var password = $form.find('input[name=password]').val();

    $(".error").hide();
    var isValid = true;

    if (login == "") {
        $(".loginEmpty").show();
        isValid = false;
    }

    if (password == "") {
        $(".passwordEmpty").show();
        isValid = false;
    }

    return isValid;
}

function submitAddCardForm(form) {
    var $form = $(form);
    $form.submit();

}

function submitDeleteAccountForm(form) {
    var $form = $(form);
    $form.submit();
}

function validateDeleteCardsForm(form) {
    var $form = $(form);
    var removeCards = $("#removeCardMessage").html();
    var count = $form.find('input[name=id]:checked').length;
    if (count == 0) {
        $(".deleteCardsError").show();
    } else if (confirm(removeCards)) {
        $form.submit();
    }
}

function validateRegistrationForm(form) {
    var $form = $(form);

    var password = $form.find('input[name=password]').val();
    var repeatPassword = $form.find('input[name=repeat_password]').val();
    var name = $form.find('input[name=name]').val();
    var surname = $form.find('input[name=surname]').val();
    var email = $form.find('input[name=email]').val();

    var regularExp = /[A-ZА-Я]{1}[a-zа-я]+$/;
    var regularExpEmail = /\w+([\.-]*\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

    $(".error").hide();

    var isValid = true;

    if (password == "") {
        $(".passwordEmpty").show();
        isValid = false;
    } else if (password.length < 6 || password.length >= 20) {
        $(".passwordError").show();
        isValid = false;
    }

    if (repeatPassword == "") {
        $(".repeatPasswordEmpty").show();
        isValid = false;
    } else if (password != repeatPassword) {
        $(".repeatPasswordError").show();
        isValid = false;
    }

    if (name == "") {
        $(".nameEmpty").show();
        isValid = false;
    } else if (!name.match(regularExp)) {
        $(".nameError").show();
        isValid = false;
    }

    if (surname == "") {
        $(".surnameEmpty").show();
        isValid = false;
    } else if (!surname.match(regularExp)) {
        $(".surnameError").show();
        isValid = false;
    }

    if (email == "") {
        $(".emailEmpty").show();
        isValid = false;
    } else if (!email.match(regularExpEmail)) {
        $(".emailError").show();
        isValid = false;
    }

    return isValid;
}

function validateAddAccountForm(form) {
    var $form = $(form);

    var initBalance = $form.find('input[name=balance]').val();

    var regularExp = /\d+$/;
    var regularExpAccNumber = /[A-Z]{1}\d{6}$/;
    $(".error").hide();
    var isValid = true;

    if (!initBalance.match(regularExp)) {
        $(".initBalanceError").show();
        isValid = false;
    }

    if (initBalance > 1000000000000) {
        $(".initBalanceMaxRangeError").show();
        isValid = false;
    }

    return isValid;
}

function validateReplenishBalanceForm(form) {
    var $form = $(form);

    var replenishAmount = $form.find('input[name=replenishAmount]').val();

    var regularExp = /\d+$/;

    $(".error").hide();

    var isValid = true;

    if (replenishAmount > 1000000000000) {
        $(".replenishAmountMaxRangeError").show();
        isValid = false;
    }

    if (replenishAmount < 0) {
        $(".replenishAmountMinRangeError").show();
        isValid = false;
    }

    if (!replenishAmount.match(regularExp)) {
        $(".replenishError").show();
        isValid = false;
    }

    if (replenishAmount < 0) {
        $(".replenishAmountMinRangeError").show();
        isValid = false;
    }

    return isValid;
}

function validateAddPaymentForm(form) {
    var $form = $(form);

    var amount = $form.find('input[name=amount]').val();
    var receiverAccount = $form.find('input[name=receiverAccount]').val();

    var regularExp = /\d+$/;
    var regularExpReceiverAcc = /[A-Z]{1}\d{6}$/;

    $(".error").hide();

    var isValid = true;

    if (!amount.match(regularExp)) {
        $(".amountError").show();
        isValid = false;
    }

    if (amount < 0) {
        $(".amountMinRangeError").show();
        isValid = false;
    }

    if (!receiverAccount.match(regularExpReceiverAcc)) {
        $(".receiverError").show();
        isValid = false;
    }
    return isValid;
}

function validateAddCurrencyForm(form) {
    var $form = $(form);

    var minAmount = $form.find('input[name=minAmount]').val();
    var currencyCode = $form.find('input[name=currencyCode]').val();

    var regularExp = /[A-Z]{3}$/;
    var regularExpAmount = /\d+$/;
    $(".error").hide();

    var isValid = true;

    if (minAmount < 0) {
        $(".currencyMinAmountError").show();
        isValid = false;
    }

    if (minAmount > 1000000) {
        $(".currencyMaxAmountError").show();
        isValid = false;
    }

    if (!minAmount.match(regularExpAmount)) {
        $(".currencyAmountFormatError").show();
        isValid = false;
    }

    if (!currencyCode.match(regularExp)) {
        $(".currencyCodeError").show();
        isValid = false;
    }

    return isValid;
}