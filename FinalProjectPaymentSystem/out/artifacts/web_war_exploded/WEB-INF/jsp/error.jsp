<%@ page isErrorPage="true" contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@include file="/WEB-INF/include/locale.jsp" %>
<html>
<head>
    <title><fmt:message key="title.error"/></title>
    <%@ include file="/WEB-INF/include/js.jsp" %>
</head>
<body>

<div class="content">
    <%@ include file="/WEB-INF/include/header.jsp" %>
    <div class="main">
        <div id="error-page">
            <h1>Oops...</h1>
            <p>Sorry, an error occurred.</p></div>
    </div>
    <div class="footer-push"></div>
</div>

<c:import url="/WEB-INF/include/footer.jsp"/>

</body>
</html>