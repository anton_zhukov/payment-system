<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/include/locale.jsp" %>
<html>
<head>
    <title><fmt:message key="title.currencies"/></title>
    <%@ include file="/WEB-INF/include/js.jsp" %>
</head>

<body>

<div class="content">
    <%@ include file="/WEB-INF/include/header.jsp" %>

    <div class="main">
        <div id="currencies-table-wrapper">
            <div class="table-header">
                <a href="/do?action=viewAddCurrency" class="icon-holder add">
                    <fmt:message key="currencies.reference.add-currency"/>
                </a>
            </div>

            <table id="currencies-table">
                <tr>
                    <th>
                        <b><fmt:message key="currencies.column-header.currency-code"/></b>
                    </th>
                    <th>
                        <b><fmt:message key="currencies.column-header.min-amount"/></b>
                    </th>
                </tr>

                <c:forEach var="currency" items="${currencies}">
                    <tr>
                        <td>
                            <c:out value="${currency.currencyCode}"/>
                        </td>
                        <td>
                            <c:out value="${currency.minAmount}"/>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
    <div class="footer-push"></div>
</div>

<c:import url="/WEB-INF/include/footer.jsp"/>
</body>
</html>
