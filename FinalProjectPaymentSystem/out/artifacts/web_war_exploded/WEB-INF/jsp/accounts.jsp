<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/include/locale.jsp" %>
<html>
<head>
    <title>
        <fmt:message key="title.accounts"/>
    </title>
    <%@ include file="/WEB-INF/include/js.jsp" %>
</head>

<body>
<div class="content">
    <%@ include file="/WEB-INF/include/header.jsp" %>

    <div class="main">
        <c:if test="${empty accounts}">
            <span>
                <fmt:message key="accounts.message.no-accounts"/>
            </span>
        </c:if>
        <%@ include file="/WEB-INF/include/errors.jsp" %>

        <div class="accounts-table-caption">
            <c:if test="${sessionScope.user.type==1}">
            <span>
                <a href="/do?action=listUsers" class="icon-holder back">
                    <fmt:message key="common.reference.back"/>
                </a>
            </span>
            </c:if>
            <h3 id="h3-accounts">
                <fmt:message key="accounts.header.accounts-table"/>
            </h3>
        </div>
        <div id="account-table-wrapper">
            <div class="table-header">
            <span>
                <c:if test="${sessionScope.user.type!=1}">
                    <a href="/do?action=viewAddAccountFields" class="icon-holder add">
                        <fmt:message key="accounts.button.add-account"/>
                    </a>
                </c:if>
            </span>
            </div>
            <table id="accounts-table">
                <tr>
                    <th>
                        <b><fmt:message key="accounts.column-header.account-number"/></b>
                    </th>
                    <th>
                        <b><fmt:message key="accounts.column-header.current-balance"/></b>
                    </th>
                    <th>
                        <b><fmt:message key="accounts.column-header.bank"/></b>
                    </th>
                    <th>
                        <b><fmt:message key="accounts.column-header.status"/></b>
                    </th>
                </tr>
                <c:forEach var="accountDetails" items="${accounts}">
                    <tr>
                        <td>
                            <a href="/do?action=listCards&accountId=${accountDetails.account.accountId}"
                               title="<fmt:message key="accounts.title.watch-account"/>">
                                    ${accountDetails.account.accountNumber}
                            </a>
                        </td>
                        <td>
                            <fmt:formatNumber type="number" maxFractionDigits="2"
                                              value="${accountDetails.account.currentBalance}"/>
                            <c:out value="${accountDetails.currency.currencyCode}"/>
                        </td>
                        <td>
                                ${accountDetails.bank.bankName}
                        </td>
                        <td>
                            <c:set var="string1" value="${accountDetails.accountStatus.statusValue}"/>
                            <c:set var="string2" value="${fn:toLowerCase(string1)}"/>
                            <fmt:message key="accounts.status.${string2}"/>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>

    </div>
    <div class="footer-push"></div>
</div>

<c:import url="/WEB-INF/include/footer.jsp"/>
</body>
</html>
