<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/include/locale.jsp" %>
<html>
<head>
    <title><fmt:message key="title.banks"/></title>
    <%@ include file="/WEB-INF/include/js.jsp" %>
</head>

<body>
<div class="content">
    <%@ include file="/WEB-INF/include/header.jsp" %>

    <div class="main">
        <div id="banks-table-wrapper">
            <div class="table-header">
                <div>
                    <a href="/do?action=viewAddBank" class="icon-holder add">
                        <fmt:message key="banks.reference.add-bank"/>
                    </a>
                </div>
            </div>
            <table id="banks-table">
                <tr>
                    <th>
                        <b><fmt:message key="banks.column-header.bank-name"/></b>
                    </th>
                </tr>

                <c:forEach var="bank" items="${banks}">
                    <tr>
                        <td>
                                ${bank.bankName}
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
    <div class="footer-push"></div>
</div>

<c:import url="/WEB-INF/include/footer.jsp"/>
</body>
</html>
