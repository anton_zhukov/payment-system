<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="/WEB-INF/include/locale.jsp" %>
<html lang="${language}">
<head>
    <title><fmt:message key="title.add-bank"/></title>
    <%@ include file="/WEB-INF/include/js.jsp" %>
</head>

<body>
<div class="content">
    <%@ include file="/WEB-INF/include/header.jsp" %>

    <div class="main">
        <div>
            <a href="/do?action=listBanks" class="icon-holder back">
                <fmt:message key="common.reference.back"/>
            </a>
        </div>
        <form action="/do?action=addBank" method="post">
            <div>
                <input type="text" placeholder="<fmt:message key="add-bank.input.bank-name"/>" name="bankName"
                       class="top" required="required"/>
            </div>
            <div>
                <fmt:message key="add-bank.button.add" var="buttonAddBank"/>
                <button type="submit" class="submit-button top">${buttonAddBank}</button>
            </div>
        </form>
    </div>
    <div class="footer-push"></div>
</div>
<c:import url="/WEB-INF/include/footer.jsp"/>
</body>
</html>