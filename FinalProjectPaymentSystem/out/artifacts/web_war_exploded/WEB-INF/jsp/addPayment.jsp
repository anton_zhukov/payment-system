<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="/WEB-INF/include/locale.jsp" %>
<html>
<head>
    <title><fmt:message key="title.add-payment"/></title>
    <%@ include file="/WEB-INF/include/js.jsp" %>
</head>

<body>

<div class="content">
    <%@ include file="/WEB-INF/include/header.jsp" %>

    <div class="main">
        <%@ include file="/WEB-INF/include/errors.jsp" %>
        <div>
            <a href="/do?action=listCards&accountId=${accountId}" class="icon-holder back">
                <fmt:message key="common.reference.back"/>
            </a>
        </div>
        <form action="/do?action=addPayment" method="post"
              onsubmit="return validateAddPaymentForm(this);">
            <div class="top">
                <input type="text" value="${requestScope.payment.amount}"
                       placeholder="<fmt:message key="add-payment.input.amount"/>" name="amount" required="required"/>
                <span class="amountError error" style="display: none;">
                    <fmt:message key="add-payment.js-validation.wrong-amount-format"/>
                </span>
                <span class="amountMinRangeError error" style="display: none;">
                    <fmt:message key="add-payment.js-validation.wrong-amount-min-range"/>
                </span>
            </div>

            <input type="hidden" name="cardId" value="${param.cardId}"/>

            <div>
                <input type="text" value="${requestScope.payment.receiverAccount}"
                       placeholder="<fmt:message key="add-payment.input.receiver-account"/>" name="receiverAccount"
                       required="required"/>
                <span class="receiverError error" style="display: none;">
                    <fmt:message key="add-payment.js-validation.wrong-receiver-account"/>
                </span>
            </div>

            <input type="hidden" name="accountId" value="${accountId}"/>

            <div>
                <button type="submit" class="submit-button top">
                    <fmt:message key="add-payment.button.pay"/>
                </button>
            </div>
        </form>

        <div class="top">
            <a href="/do?action=viewReplenishBalancePage&accountId=${accountId}&currencyId=${param.currencyId}"
               class="icon-holder replenish">
                <fmt:message key="common.reference.move-to-replenish-balance"/>
            </a>
        </div>
    </div>
    <div class="footer-push"></div>
</div>
<c:import url="/WEB-INF/include/footer.jsp"/>
</body>
</html>