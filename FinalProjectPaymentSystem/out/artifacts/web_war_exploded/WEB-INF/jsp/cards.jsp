<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/include/locale.jsp" %>
<html>
<head>
    <title><fmt:message key="title.cards"/></title>
    <%@ include file="/WEB-INF/include/js.jsp" %>
</head>
<body>

<div class="content">
    <%@ include file="/WEB-INF/include/header.jsp" %>

    <div class="main">
        <%@ include file="/WEB-INF/include/errors.jsp" %>

        <%@ include file="/WEB-INF/include/accountDetails.jsp" %>

        <div id="card-details-wrapper">
            <c:if test="${sessionScope.user.type==1}">
                <div id="admin-back-to-accounts">
                    <a href="/do?action=listAccounts&userId=${accountDetails.account.userId}" class="icon-holder back">
                        <fmt:message key="common.reference.back"/>
                    </a>
                </div>
            </c:if>
            <c:choose>
                <c:when test="${not empty cards}">
                    <div class="table-header">
                        <c:if test="${accountDetails.accountStatus.statusId==1}">
                            <%@ include file="/WEB-INF/include/addNewCard.jsp" %>
                            <c:if test="${sessionScope.user.type==0}">
                                    <span id="removeCardMessage" style="display: none;">
                                        <fmt:message key="cards.confirm.remove-cards"/>
                                    </span>
                                <a href=""
                                   onClick="validateDeleteCardsForm(document.forms['deleteCards']); return false;"
                                   class="icon-holder delete">
                                    <fmt:message key="cards.reference.delete-chosen-cards"/>
                                </a>
                            </c:if>
                            <span class="deleteCardsError error" style="display: none;">
                                <fmt:message key="cards.js-validation.no-cards-selected"/>
                            </span>
                        </c:if>
                    </div>
                    <form action="/do?action=deleteCard&accountId=${param.accountId}" method="post" id="deleteCards">
                        <table id="card-details">
                            <tr>
                                <th class="checkbox">
                                </th>
                                <th>
                                    <fmt:message key="cards.column-header.card-number"/>
                                </th>
                                <th>
                                    <fmt:message key="cards.column-header.expiration-date"/>
                                </th>
                            </tr>
                            <c:forEach var="card" items="${cards}">
                                <tr>
                                    <td>
                                        <input type="checkbox" name="id" value="${card.cardId}">
                                    </td>
                                    <td>
                                        <fmt:formatNumber type="number" pattern="####,####,####,####"
                                                          value="${card.cardNumber}"/>
                                    </td>
                                    <td>
                                        <fmt:formatDate value="${card.expirationDate}" pattern="MM/yyyy"/>
                                    </td>
                                    <td>
                                        <c:if test="${sessionScope.user.type==0 && accountDetails.accountStatus.statusId==1}">
                                            <div>
                                                <a href="/do?action=listPaymentsByCard&cardId=${card.cardId}&accountId=${card.accountId}"
                                                   class="icon-holder view">
                                                    <fmt:message key="cards.reference.watch-payments-by-card"/>
                                                </a>
                                            </div>
                                            <div>
                                                <a href="/do?action=viewAddPayment&cardId=${card.cardId}&currencyId=${accountDetails.currency.currencyId}"
                                                   class="icon-holder pay">
                                                    <fmt:message key="cards.reference.move-to-add-payment"/>
                                                </a>
                                            </div>
                                        </c:if>
                                    </td>
                                </tr>
                            </c:forEach>
                        </table>
                    </form>
                </c:when>
                <c:otherwise>
                    <div>
                        <fmt:message key="cards.message.no-cards"/>
                    </div>
                    <c:choose>
                        <c:when test="${accountDetails.accountStatus.statusValue eq 'Pending' && (sessionScope.user.type!=1)}">
                            <div>
                                <fmt:message key="cards.message.pending"/>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <%@ include file="/WEB-INF/include/addNewCard.jsp" %>
                        </c:otherwise>
                    </c:choose>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
    <div class="footer-push"></div>
</div>

<c:import url="/WEB-INF/include/footer.jsp"/>
</body>
</html>