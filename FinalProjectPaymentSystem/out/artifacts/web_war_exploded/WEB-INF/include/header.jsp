<%@ taglib prefix="ctg" uri="customtags" %>
<div id="change-locale">
    <form action="/do?action=changeLocale" method="post" id="changeLocaleForm">

        <select name="locale" class="select">
            <option value="en_US" <c:if test="${sessionScope.userLocale eq 'en_US'}">selected</c:if>>
                ENG
            </option>
            <option value="ru_RU" <c:if test="${sessionScope.userLocale eq 'ru_RU'}">selected</c:if>>
                RU
            </option>
        </select>
    </form>
</div>

<c:if test="${not empty sessionScope.user}">
    <div>
        <div id="logout">
            <a href="/do?action=userLogout" class="icon-holder log-out">
                <fmt:message key="common.button.log-out"/>
            </a>
        </div>
        <div id="greeting">
            <fmt:message key="accounts.message.greeting" var="greetmessage"/>
            <ctg:hello hello="${greetmessage}" name="${sessionScope.user.name}" surname="${sessionScope.user.surname}"/>
        </div>
        <div id="back-to-main-page">
            <a href="/do?action=viewLogin" class="icon-holder home">
                <fmt:message key="common.reference.home"/>
            </a>
        </div>
    </div>
    <hr/>
</c:if>