<div id="account-details-wrapper">
    <table id="account-details">
        <tr>
            <th>
                <h3 align="center">
                    <fmt:message key="cards.column-header.account-details"/>
                </h3>
            </th>
        </tr>
        <tr>
            <td>
                <fmt:formatNumber type="number" maxFractionDigits="2"
                                  value="${accountDetails.account.currentBalance}"/>
                ${accountDetails.currency.currencyCode}
            </td>
        </tr>
        <tr>
            <td>
                ${accountDetails.bank.bankName}
            </td>
        </tr>
        <tr>
            <td>
                <c:set var="string1" value="${accountDetails.accountStatus.statusValue}"/>
                <c:set var="string2" value="${fn:toLowerCase(string1)}"/>
                <fmt:message key="accounts.status.${string2}"/>
            </td>
        </tr>
    </table>

    <c:choose>
        <c:when test="${sessionScope.user.type==1}">
            <c:if test="${accountDetails.accountStatus.statusId!=1}">
                <div>
                    <a href="/do?action=activateAccount&accountId=${param.accountId}&userId=${accountDetails.account.userId}"
                       class="icon-holder activate">
                        <fmt:message key="cards.reference.account-activate"/>
                    </a>
                </div>
            </c:if>
        </c:when>

        <c:otherwise>
            <c:if test="${accountDetails.accountStatus.statusId==1}">
                <div class="top">
                    <a href="/do?action=viewReplenishBalancePage&accountId=${param.accountId}&currencyId=${accountDetails.currency.currencyId}"
                       class="icon-holder replenish">
                        <fmt:message key="common.reference.move-to-replenish-balance"/>
                    </a>
                </div>
                <div class="top">
                    <a href="/do?action=listPaymentsByAccount&accountId=${param.accountId}"
                       class="icon-holder view">
                        <fmt:message key="cards.reference.watch-payments-by-account"/>
                    </a>
                </div>
                <div class="top">
                    <fmt:message key="cards.confirm.block-account" var="blockAccount"/>
                    <a href="/do?action=blockAccount&accountId=${param.accountId}"
                       class="icon-holder block"
                       onClick="return confirm('${blockAccount}');">
                        <fmt:message key="cards.reference.account-block"/>
                    </a>
                </div>
                <div class="top">
                    <fmt:message key="cards.confirm.remove-account" var="removeAccount"/>
                    <form action="/do?action=deleteAccount" method="post"
                          onSubmit="return confirm('${removeAccount}'); return false;"
                          id="deleteAccount">
                        <input type="hidden" name="accountId" value="${param.accountId}"/>
                        <a href="#" class="icon-holder delete"
                           onClick="submitDeleteAccountForm(document.forms['deleteAccount']);">
                            <fmt:message key="cards.reference.account-delete"/>
                        </a>
                    </form>
                </div>
            </c:if>
        </c:otherwise>
    </c:choose>
</div>