<c:set var="language" value="${not empty sessionScope.userLocale ? sessionScope.userLocale : 'en_US'}" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties.text" />
