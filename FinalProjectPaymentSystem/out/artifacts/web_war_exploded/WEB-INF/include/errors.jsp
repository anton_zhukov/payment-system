<c:if test="${(not empty errorHolder) && errorHolder.haveErrors()}">
    <div class="error-messages">
        <c:forEach var="err" items="${errorHolder.getErrors()}">
            <fmt:message key="${err.key}">
                <c:forEach var="value" items="${err.values}">
                    <fmt:param value="${value}"/>
                </c:forEach>
            </fmt:message>
        </c:forEach>
    </div>
</c:if>