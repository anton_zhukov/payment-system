<c:choose>
    <c:when test="${sessionScope.user.type==1}">
    </c:when>
    <c:otherwise>
        <fmt:message key="cards.confirm.add-card" var="addCard"/>
        <form action="/do?action=addCard" method="post" onSubmit="return confirm('${addCard}'); return false;"
              id="addCard">
            <input type="hidden" name="accountId" value="${param.accountId}"/>
            <a href="#" class="icon-holder add" onClick="submitAddCardForm(document.forms['addCard']);">
                <fmt:message key="cards.reference.add-card"/>
            </a>
        </form>
    </c:otherwise>
</c:choose>