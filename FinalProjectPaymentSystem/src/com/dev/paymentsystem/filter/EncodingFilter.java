package com.dev.paymentsystem.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * Filter for encoding characters on every request
 */
public class EncodingFilter implements Filter {

    private static final String ENCODING = "encoding";

    String code;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        code = filterConfig.getInitParameter(ENCODING);
    }

    /**
     * @param servletRequest
     * @param servletResponse
     * @param filterChain     The filter chain we are processing
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {

        String codeRequest = servletRequest.getCharacterEncoding();
        if (code != null && !code.equalsIgnoreCase(codeRequest)) {
            servletRequest.setCharacterEncoding(code);
            servletResponse.setCharacterEncoding(code);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}