package com.dev.paymentsystem.constants;

public class Pages {

    public static final String HOME_PAGE = "/";
    public static final String LOGIN_PAGE = "/WEB-INF/jsp/login.jsp";
    public static final String LIST_CARDS_PAGE = "/WEB-INF/jsp/cards.jsp";
    public static final String REGISTRATION_PAGE = "/WEB-INF/jsp/registration.jsp";
    public static final String POST_REGISTRATION_PAGE = "/WEB-INF/jsp/post_registration.jsp";
    public static final String ADD_ACCOUNT_PAGE = "/WEB-INF/jsp/addAccount.jsp";
    public static final String ADD_PAYMENT_PAGE = "/WEB-INF/jsp/addPayment.jsp";
    public static final String LIST_ACCOUNTS_PAGE = "/WEB-INF/jsp/accounts.jsp";
    public static final String LIST_PAYMENTS = "/WEB-INF/jsp/payments.jsp";
    public static final String REPLENISH_BALANCE_PAGE = "/WEB-INF/jsp/replenishBalance.jsp";
    public static final String ADMIN_PAGE = "/WEB-INF/jsp/admin.jsp";
    public static final String USERS_PAGE = "/WEB-INF/jsp/users.jsp";
    public static final String BANKS_PAGE = "/WEB-INF/jsp/banks.jsp";
    public static final String CURRENCIES_PAGE = "/WEB-INF/jsp/currencies.jsp";
    public static final String ADD_BANK_PAGE = "/WEB-INF/jsp/addBank.jsp";
    public static final String ADD_CURRENCY_PAGE = "/WEB-INF/jsp/addCurrency.jsp";
}