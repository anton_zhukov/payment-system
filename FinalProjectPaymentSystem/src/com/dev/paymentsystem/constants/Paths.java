package com.dev.paymentsystem.constants;

public class Paths {

    public static final String LIST_ACCOUNTS = "/do?action=listAccounts";
    public static final String LIST_CARDS = "/do?action=listCards&accountId=%d";
    public static final String LIST_CURRENCIES = "/do?action=listCurrencies";
    public static final String VIEW_ADD_ACCOUNT = "/do?action=viewAddAccountFields";
    public static final String LIST_BANKS = "/do?action=listBanks";
    public static final String LIST_PAYMENTS_BY_CARD = "/do?action=listPaymentsByCard&cardId=%d&accountId=%d";
    public static final String VIEW_ADD_PAYMENT = "/do?action=viewAddPayment&cardId=%d&accountId=%d";
    public static final String VIEW_REGISTRATION = "/do?action=viewRegistration";
    public static final String VIEW_POST_REGISTRATION = "/do?action=viewPostRegistration";
    public static final String VIEW_LOGIN = "/do?action=viewLogin";
}