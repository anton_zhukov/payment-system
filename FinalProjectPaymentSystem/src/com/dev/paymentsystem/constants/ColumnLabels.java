package com.dev.paymentsystem.constants;

public class ColumnLabels {

    public static final String BALANCE = "balance";
    public static final String ACCOUNT_ID = "account_id";
    public static final String BANK_ID = "bank_id";
    public static final String BANK_NAME = "bank_name";
    public static final String CURRENCY_ID = "currency_id";
    public static final String CURRENCY_CODE = "currency_code";
    public static final String MIN_AMOUNT = "min_amount";
    public static final String STATUS_ID = "status_id";
    public static final String STATUS_VALUE = "status_value";
    public static final String ACCOUNT_NUMBER = "account_number";
    public static final String USER_ID = "user_id";
    public static final String LOGIN = "login";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String TYPE = "type";
    public static final String EMAIL = "email";
    public static final String CARD_ID = "card_id";
    public static final String CARD_NUMBER = "card_number";
    public static final String EXPIRATION_DATE = "expiration_date";
    public static final String PAYMENT_ID = "payment_id";
    public static final String AMOUNT = "amount";
    public static final String RECEIVER_ACCOUNT = "receiver_account";
    public static final String PAYMENT_DATE = "payment_date";
}