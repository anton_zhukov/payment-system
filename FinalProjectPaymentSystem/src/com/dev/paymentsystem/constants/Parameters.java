package com.dev.paymentsystem.constants;

public class Parameters {

    public static final String USER_ID = "userId";
    public static final String ACCOUNT_ID = "accountId";
    public static final String BANK = "bank";
    public static final String BANK_NAME = "bankName";
    public static final String BALANCE = "balance";
    public static final String CURRENCY = "currency";
    public static final String CURRENCY_CODE = "currencyCode";
    public static final String MIN_AMOUNT = "minAmount";
    public static final String REPLENISH_AMOUNT = "replenishAmount";
    public static final String LOCALE = "locale";
    public static final String ACTION = "action";
    public static final String CARD_ID = "cardId";
    public static final String CARDS_IDS = "id";
    public static final String AMOUNT = "amount";
    public static final String RECEIVER_ACCOUNT = "receiverAccount";

    public static final String PASSWORD = "password";
    public static final String REPEAT_PASSWORD = "repeat_password";
    public static final String LOGIN = "login";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String EMAIL = "email";
}