package com.dev.paymentsystem.constants;


public class AccountStatus {

    public static final int ACTIVE = 1;
    public static final int BLOCKED = 2;
    public static final int PENDING = 3;
}