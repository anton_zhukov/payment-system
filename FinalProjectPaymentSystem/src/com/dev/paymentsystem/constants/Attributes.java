package com.dev.paymentsystem.constants;

public class Attributes {

    public static final String ERROR_HOLDER = "errorHolder";
    public static final String USER = "user";
    public static final String USERS = "users";
    public static final String ACCOUNT = "account";
    public static final String ACCOUNT_ID = "accountId";
    public static final String ACCOUNTS = "accounts";
    public static final String ACCOUNT_DETAILS = "accountDetails";
    public static final String BANKS = "banks";
    public static final String CARDS = "cards";
    public static final String CURRENCY = "currency";
    public static final String CURRENCIES = "currencies";
    public static final String PAYMENT = "payment";
    public static final String PAYMENTS = "payments";
    public static final String REPLENISH_AMOUNT = "replenishAmount";
    public static final String USER_LOCALE = "userLocale";
    public static final String LOGIN = "login";
}