package com.dev.paymentsystem.constants;


public class UserTypes {

    public static final int CLIENT = 0;
    public static final int ADMIN = 1;
}