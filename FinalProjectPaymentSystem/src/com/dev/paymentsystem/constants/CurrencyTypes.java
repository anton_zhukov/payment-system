package com.dev.paymentsystem.constants;

public class CurrencyTypes {

    private static final int BYR = 1;
    private static final int EUR = 2;
    private static final int USD = 3;
    private static final int GBP = 4;
    private static final int UAH = 7;
    private static final int RUB = 8;
}
