package com.dev.paymentsystem.constants;

public class CommandKeys {

    public static final String USER_LOGIN = "userLogin";
    public static final String USER_LOGOUT = "userLogout";
    public static final String REGISTRATE_USER = "registrateUser";
    public static final String VIEW_REGISTRATION = "viewRegistration";
    public static final String VIEW_POST_REGISTRATION = "viewPostRegistration";
    public static final String VIEW_LOGIN = "viewLogin";
    public static final String VIEW_ADMIN = "viewAdmin";
    public static final String LIST_USERS = "listUsers";

    public static final String LIST_BANKS = "listBanks";
    public static final String ADD_BANK = "addBank";
    public static final String VIEW_ADD_BANK = "viewAddBank";

    public static final String LIST_CURRENCIES = "listCurrencies";
    public static final String ADD_CURRENCY = "addCurrency";
    public static final String VIEW_ADD_CURRENCY = "viewAddCurrency";

    public static final String CHANGE_LOCALE = "changeLocale";

    public static final String ADD_CARD = "addCard";
    public static final String DELETE_CARD = "deleteCard";
    public static final String LIST_CARDS = "listCards";

    public static final String LIST_PAYMENTS_BY_CARD = "listPaymentsByCard";
    public static final String LIST_PAYMENTS_BY_ACCOUNT = "listPaymentsByAccount";
    public static final String ADD_PAYMENT = "addPayment";
    public static final String VIEW_ADD_PAYMENT = "viewAddPayment";

    public static final String ADD_ACCOUNT = "addAccount";
    public static final String LIST_ACCOUNTS = "listAccounts";
    public static final String ACTIVATE_ACCOUNT = "activateAccount";
    public static final String BLOCK_ACCOUNT = "blockAccount";
    public static final String DELETE_ACCOUNT = "deleteAccount";
    public static final String VIEW_ADD_ACCOUNT_FIELDS = "viewAddAccountFields";
    public static final String REPLENISH_BALANCE = "replenishBalance";
    public static final String VIEW_REPLENISH_BALANCE_PAGE = "viewReplenishBalancePage";
}