package com.dev.paymentsystem.controller.action;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ForwardAction extends Action {

    public ForwardAction(String path) {
        super(path);
    }

    /**
     * Forwards request to the target path
     *
     * @param request  HttpServletRequest instance
     * @param response HttpServletResponse instance
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void go(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher(path).forward(request, response);
    }
}