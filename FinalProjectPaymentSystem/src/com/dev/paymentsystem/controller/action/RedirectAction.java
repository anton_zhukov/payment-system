package com.dev.paymentsystem.controller.action;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RedirectAction extends Action {

    public RedirectAction(String path) {
        super(path);
    }

    /**
     * Sends the response to the browser.
     * Then the browser makes another request to the new path
     *
     * @param request  HttpServletRequest instance
     * @param response HttpServletResponse instance
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void go(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect(path);
    }
}