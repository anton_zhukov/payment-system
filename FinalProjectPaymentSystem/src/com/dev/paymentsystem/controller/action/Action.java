package com.dev.paymentsystem.controller.action;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class Action {

    protected String path;

    public Action(String path) {
        this.path = path;
    }

    /**
     * @param request  HttpServletRequest instance
     * @param response HttpServletResponse instance
     * @throws ServletException
     * @throws IOException
     */
    public abstract void go(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
}