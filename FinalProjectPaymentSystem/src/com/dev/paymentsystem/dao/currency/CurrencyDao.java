package com.dev.paymentsystem.dao.currency;

import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.Currency;

import java.util.List;

public interface CurrencyDao {

    /**
     * Adds new currency to database
     *
     * @param currency Currency instance for getting column values
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    void addCurrency(Currency currency) throws DaoException;

    /**
     * Gets list of currencies
     *
     * @return new ArrayList containing currencies' information
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    List<Currency> listCurrencies() throws DaoException;

    /**
     * Fetches min amount for creating account with definite currency
     *
     * @param currencyId id of definite currency
     * @return min amount for definite currency
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    double fetchMinAmount(int currencyId) throws DaoException;
}