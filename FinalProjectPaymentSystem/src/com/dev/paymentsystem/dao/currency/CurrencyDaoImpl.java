package com.dev.paymentsystem.dao.currency;

import com.dev.paymentsystem.constants.ColumnLabels;
import com.dev.paymentsystem.dao.Dao;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.Currency;
import com.dev.paymentsystem.pool.ConnectionPool;
import com.dev.paymentsystem.pool.ConnectionProxy;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CurrencyDaoImpl extends Dao implements CurrencyDao {

    private static final String ADD_CURRENCY = "INSERT INTO currency (currency_code, min_amount) VALUES (?, ?);";
    private static final String LIST_CURRENCY = "SELECT currency_id, currency_code, min_amount FROM currency;";
    private static final String FETCH_MIN_AMOUNT = "SELECT min_amount FROM currency WHERE currency_id=?;";

    /**
     * Adds new currency to database
     *
     * @param currency Currency instance for getting column values
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public void addCurrency(Currency currency) throws DaoException {
        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(ADD_CURRENCY);
            ps.setString(1, currency.getCurrencyCode());
            ps.setDouble(2, currency.getMinAmount());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by adding new currency in database", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * Gets list of currencies
     *
     * @return new ArrayList containing currencies' information
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public List<Currency> listCurrencies() throws DaoException {
        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        List<Currency> currencies = null;
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(LIST_CURRENCY);
            ResultSet rs = ps.executeQuery();
            currencies = new ArrayList<>();
            while (rs.next()) {
                currencies.add(setupCurrencyFields(rs));
            }
        } catch (SQLException e) {
            throw new DaoException("Error by getting list of currencies", e);
        } finally {
            closeResources(connection, ps);
        }
        return currencies;
    }

    /**
     * Fetches min amount for creating account with definite currency
     *
     * @param currencyId id of definite currency
     * @return min amount for definite currency
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public double fetchMinAmount(int currencyId) throws DaoException {
        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        double minAmount = 0;
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(FETCH_MIN_AMOUNT);
            ps.setInt(1, currencyId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                minAmount = rs.getDouble(ColumnLabels.MIN_AMOUNT);
            }
        } catch (SQLException e) {
            throw new DaoException("Error by fetching min amount from currency table", e);
        } finally {
            closeResources(connection, ps);
        }
        return minAmount;
    }

    /**
     * Sets up fields of Currency instance
     *
     * @param rs ResultSet instance
     * @return new Currency
     * @throws SQLException
     */
    private Currency setupCurrencyFields(ResultSet rs) throws SQLException {

        Currency currency = new Currency();
        currency.setCurrencyId(rs.getInt(ColumnLabels.CURRENCY_ID));
        currency.setCurrencyCode(rs.getString(ColumnLabels.CURRENCY_CODE));
        currency.setMinAmount(rs.getDouble(ColumnLabels.MIN_AMOUNT));
        return currency;
    }
}