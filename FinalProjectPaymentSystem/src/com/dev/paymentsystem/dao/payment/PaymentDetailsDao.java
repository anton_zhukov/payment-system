package com.dev.paymentsystem.dao.payment;

import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.PaymentDetails;

import java.util.List;

public interface PaymentDetailsDao {

    /**
     * Gets list of PaymentDetails by card
     *
     * @param cardId id of definite credit-card
     * @return new ArrayList containing details about payments by card
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    List<PaymentDetails> listPaymentsByCard(int cardId) throws DaoException;

    /**
     * Gets list of PaymentDetails by account
     *
     * @param accountId id of definite account
     * @return new ArrayList containing details about payments by account
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    List<PaymentDetails> listPaymentsByAccount(int accountId) throws DaoException;
}