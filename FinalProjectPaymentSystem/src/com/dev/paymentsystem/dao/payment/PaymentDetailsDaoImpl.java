package com.dev.paymentsystem.dao.payment;

import com.dev.paymentsystem.constants.ColumnLabels;
import com.dev.paymentsystem.dao.Dao;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.CreditCard;
import com.dev.paymentsystem.entity.Currency;
import com.dev.paymentsystem.entity.Payment;
import com.dev.paymentsystem.entity.PaymentDetails;
import com.dev.paymentsystem.pool.ConnectionPool;
import com.dev.paymentsystem.pool.ConnectionProxy;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PaymentDetailsDaoImpl extends Dao implements PaymentDetailsDao {

    private static final String LIST_PAYMENTS_BY_CARD_ID = "SELECT pay.payment_id, pay.amount, pay.receiver_account, pay.payment_date,\n" +
            "pay.card_id, card.card_number, card.account_id, card.expiration_date,\n" +
            "curr.currency_id, curr.currency_code, curr.min_amount\n" +
            "FROM payment AS pay \n" +
            "INNER JOIN creditcard AS card\n" +
            "ON pay.card_id=card.card_id\n" +
            "INNER JOIN account\n" +
            "ON account.account_id=card.account_id\n" +
            "INNER JOIN currency AS curr\n" +
            "ON account.currency_id=curr.currency_id\n" +
            "WHERE pay.card_id=? \n" +
            "ORDER BY payment_date DESC;";

    private static final String LIST_PAYMENTS_BY_ACCOUNT_ID = "SELECT pay.payment_id, pay.amount, pay.receiver_account, pay.payment_date,\n" +
            "pay.card_id, card.card_number, card.account_id, card.expiration_date,\n" +
            "curr.currency_id, curr.currency_code, curr.min_amount\n" +
            "FROM payment AS pay \n" +
            "INNER JOIN creditcard AS card\n" +
            "ON pay.card_id=card.card_id\n" +
            "INNER JOIN account\n" +
            "ON account.account_id=card.account_id\n" +
            "INNER JOIN currency AS curr\n" +
            "ON account.currency_id=curr.currency_id\n" +
            "WHERE card.account_id=? \n" +
            "ORDER BY payment_date DESC;";

    /**
     * Gets list of PaymentDetails by card
     *
     * @param cardId id of definite credit-card
     * @return new ArrayList containing details about payments by card
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public List<PaymentDetails> listPaymentsByCard(int cardId) throws DaoException {
        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        List<PaymentDetails> payments = null;
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(LIST_PAYMENTS_BY_CARD_ID);
            ps.setInt(1, cardId);
            ResultSet rs = ps.executeQuery();
            payments = new ArrayList<>();
            while (rs.next()) {
                payments.add(setupPaymentFields(rs));
            }
        } catch (SQLException e) {
            throw new DaoException("Error by getting list of payment details by card-id", e);
        } finally {
            closeResources(connection, ps);
        }
        return payments;
    }

    /**
     * Gets list of PaymentDetails by account
     *
     * @param accountId id of definite account
     * @return new ArrayList containing details about payments by account
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public List<PaymentDetails> listPaymentsByAccount(int accountId) throws DaoException {
        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        List<PaymentDetails> payments = null;
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(LIST_PAYMENTS_BY_ACCOUNT_ID);
            ps.setInt(1, accountId);
            ResultSet rs = ps.executeQuery();
            payments = new ArrayList<>();
            while (rs.next()) {
                payments.add(setupPaymentFields(rs));
            }
        } catch (SQLException e) {
            throw new DaoException("Error by getting list of payment details by account-id", e);
        } finally {
            closeResources(connection, ps);
        }
        return payments;
    }

    /**
     * Sets up fields of PaymentDetails instance
     *
     * @param rs ResultSet instance
     * @return new PaymentDetails
     * @throws SQLException
     */
    private PaymentDetails setupPaymentFields(ResultSet rs) throws SQLException {

        Payment payment = new Payment();
        payment.setPaymentId(rs.getInt(ColumnLabels.PAYMENT_ID));
        payment.setAmount(rs.getDouble(ColumnLabels.AMOUNT));
        payment.setCardId(rs.getInt(ColumnLabels.CARD_ID));
        payment.setReceiverAccount(rs.getString(ColumnLabels.RECEIVER_ACCOUNT));
        payment.setPaymentDate(rs.getTimestamp(ColumnLabels.PAYMENT_DATE));

        Currency currency = new Currency();
        currency.setCurrencyId(rs.getInt(ColumnLabels.CURRENCY_ID));
        currency.setCurrencyCode(rs.getString(ColumnLabels.CURRENCY_CODE));
        currency.setMinAmount(rs.getDouble(ColumnLabels.MIN_AMOUNT));

        CreditCard creditCard = new CreditCard();
        creditCard.setCardId(rs.getInt(ColumnLabels.CARD_ID));
        creditCard.setCardNumber(rs.getLong(ColumnLabels.CARD_NUMBER));
        creditCard.setAccountId(rs.getInt(ColumnLabels.ACCOUNT_ID));
        creditCard.setExpirationDate(new Date(rs.getDate(ColumnLabels.EXPIRATION_DATE).getTime()));

        PaymentDetails details = new PaymentDetails();
        details.setPayment(payment);
        details.setCurrency(currency);
        details.setCreditCard(creditCard);

        return details;
    }
}