package com.dev.paymentsystem.dao.payment;

import com.dev.paymentsystem.dao.Dao;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.Payment;
import com.dev.paymentsystem.pool.ConnectionPool;
import com.dev.paymentsystem.pool.ConnectionProxy;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PaymentDaoImpl extends Dao implements PaymentDao {

    private static final Logger LOG = Logger.getLogger(PaymentDaoImpl.class);

    private static final String CREATE_PAYMENT = "INSERT INTO payment (amount, " +
            "card_id, receiver_account, payment_date) VALUES (?, ?, ?, ?);";
    private static final String CHARGE_OFF_BALANCE = "UPDATE account SET balance=balance-? WHERE account_id=?;";

    /**
     * Method for adding new payment to database
     *
     * @param payment   Payment instance for getting column values
     * @param accountId id of definite account
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public void addPayment(Payment payment, int accountId) throws DaoException {

        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement updatePayments = null;
        PreparedStatement updateBalance = null;
        try {
            connection.setAutoCommit(false);

            updatePayments = connection.prepareStatement(CREATE_PAYMENT);
            createPayment(updatePayments, payment);

            updateBalance = connection.prepareStatement(CHARGE_OFF_BALANCE);
            chargeOffBalance(updateBalance, accountId, payment.getAmount());

            connection.commit();
        } catch (SQLException e) {
            if (connection != null) {
                try {
                    LOG.error("Transaction is being rolled back");
                    connection.rollback();
                } catch (SQLException ex) {
                    LOG.error("Error by transaction rolling back", ex);
                }
            }
            throw new DaoException("Error by adding payment to database", e);
        } finally {
            if (connection != null) {
                try {
                    connection.setAutoCommit(true);
                } catch (SQLException e) {
                    LOG.error("Error by changing auto-commit status", e);
                }
            }
            closeResources(connection, updatePayments, updateBalance);
        }
    }

    /**
     * Creates payment that we want to add
     *
     * @param updatePayments PreparedStatement instance
     * @param payment        Payment instance for adding
     * @throws SQLException
     */
    private void createPayment(PreparedStatement updatePayments, Payment payment) throws SQLException {
        updatePayments.setDouble(1, payment.getAmount());
        updatePayments.setInt(2, payment.getCardId());
        updatePayments.setString(3, payment.getReceiverAccount());
        updatePayments.setTimestamp(4, payment.getPaymentDate());
        updatePayments.executeUpdate();
    }

    /**
     * Decreases account balance by payment
     *
     * @param accountId     id of definite account, which balance we want to decrease
     * @param paymentAmount payment's amount
     * @throws SQLException
     */
    private void chargeOffBalance(PreparedStatement updateBalance, int accountId, double paymentAmount) throws SQLException {
        updateBalance.setDouble(1, paymentAmount);
        updateBalance.setInt(2, accountId);
        updateBalance.executeUpdate();
    }
}