package com.dev.paymentsystem.dao.payment;

import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.Payment;

import java.util.List;

public interface PaymentDao {

    /**
     * Method for adding new payment to database
     *
     * @param payment Payment instance for getting column values
     * @param accountId Payment instance for getting column values
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    void addPayment(Payment payment, int accountId) throws DaoException;
}