package com.dev.paymentsystem.dao;

import com.dev.paymentsystem.pool.ConnectionPool;
import com.dev.paymentsystem.pool.ConnectionProxy;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Dao {

    private static final Logger LOG = Logger.getLogger(Dao.class);

    protected static final ConnectionPool pool = ConnectionPool.getInstance();

    /**
     * Releases connection
     *
     * @param connection connection, that's able to be released
     */
    protected void freeConnection(ConnectionProxy connection) {
        pool.releaseConnection(connection);
    }

    /**
     * Closes PreparedStatement(-s) and ConnectionProxy instances
     *
     * @param connection ConnectionProxy instance
     * @param statements PreparedStatement instance(-s)
     */
    protected void closeResources(ConnectionProxy connection, PreparedStatement...statements) {
        try {
            if (statements != null) {
                for (PreparedStatement statement : statements)
                statement.close();
            }
        } catch (SQLException e) {
            LOG.error("Error by closing prepared statement(-s) and connection", e);
        }
        freeConnection(connection);
    }
}
