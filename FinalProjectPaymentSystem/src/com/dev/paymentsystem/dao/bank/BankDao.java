package com.dev.paymentsystem.dao.bank;

import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.Bank;

import java.util.List;

public interface BankDao {

    /**
     * Adds new bank to database
     *
     * @param bank Bank instance for getting column values
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    void addBank(Bank bank) throws DaoException;

    /**
     * Gets list of banks
     *
     * @return new ArrayList containing banks' information
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    List<Bank> listBanks() throws DaoException;
}