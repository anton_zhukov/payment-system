package com.dev.paymentsystem.dao.bank;

import com.dev.paymentsystem.constants.ColumnLabels;
import com.dev.paymentsystem.dao.Dao;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.Bank;
import com.dev.paymentsystem.pool.ConnectionPool;
import com.dev.paymentsystem.pool.ConnectionProxy;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BankDaoImpl extends Dao implements BankDao {

    private static final String ADD_BANK = "INSERT INTO bank (bank_name) VALUES (?);";
    private static final String LIST_BANKS = "SELECT bank_id, bank_name FROM bank;";

    /**
     * Adds new bank to database
     *
     * @param bank Bank instance for getting column values
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public void addBank(Bank bank) throws DaoException {
        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(ADD_BANK);
            ps.setString(1, bank.getBankName());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by adding bank to database", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * Gets list of banks
     *
     * @return new ArrayList containing banks' information
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public List<Bank> listBanks() throws DaoException {
        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        List<Bank> banks = null;
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(LIST_BANKS);
            ResultSet rs = ps.executeQuery();
            banks = new ArrayList<>();
            while (rs.next()) {
                banks.add(setupBankFields(rs));
            }
        } catch (SQLException e) {
            throw new DaoException("Error by getting list of banks", e);
        } finally {
            closeResources(connection, ps);
        }
        return banks;
    }

    /**
     * Sets up fields of Bank instance
     *
     * @param rs ResultSet instance
     * @return new Bank
     * @throws SQLException
     */
    private Bank setupBankFields(ResultSet rs) throws SQLException {
        Bank bank = new Bank();
        bank.setBankId(rs.getInt(ColumnLabels.BANK_ID));
        bank.setBankName(rs.getString(ColumnLabels.BANK_NAME));
        return bank;
    }
}