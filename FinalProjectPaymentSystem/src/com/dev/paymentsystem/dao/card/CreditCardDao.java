package com.dev.paymentsystem.dao.card;

import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.CreditCard;

import java.util.List;

public interface CreditCardDao {

    /**
     * Adds new credit-card to database
     *
     * @param card CreditCard instance for getting column values
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    void addCard(CreditCard card) throws DaoException;

    /**
     * Deletes cards from database
     *
     * @param cardIds ids of cards for deletion
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    void deleteChosenCards(int[] cardIds) throws DaoException;

    /**
     * Gets list of credit cards
     *
     * @param accountId account id which cards we are interested in
     * @return new ArrayList of credit-cards
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    List<CreditCard> listCards(int accountId) throws DaoException;

    /**
     * Fetches account id by card id
     *
     * @param cardId id of definite card
     * @return id of account that is connected with definite card
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    int fetchAccountIdByCardId(int cardId) throws DaoException;
}