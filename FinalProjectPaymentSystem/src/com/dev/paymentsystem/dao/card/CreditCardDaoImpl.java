package com.dev.paymentsystem.dao.card;

import com.dev.paymentsystem.constants.ColumnLabels;
import com.dev.paymentsystem.dao.Dao;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.CreditCard;
import com.dev.paymentsystem.pool.ConnectionPool;
import com.dev.paymentsystem.pool.ConnectionProxy;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CreditCardDaoImpl extends Dao implements CreditCardDao {

    private static final String ADD_CARD = "INSERT INTO creditcard (card_number, account_id, expiration_date) VALUES (?,?,?);";
    private static final String DELETE_CHOSEN_CARDS = "DELETE FROM creditcard WHERE card_id IN(";
    private static final String LIST_CARDS = "SELECT * FROM creditcard WHERE account_id=?;";
    private static final String FETCH_ACCOUNT_ID_BY_CARD_ID = "SELECT creditcard.account_id FROM creditcard WHERE creditcard.card_id=?;";

    /**
     * Adds new credit-card to database
     *
     * @param card CreditCard instance for getting column values
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public void addCard(CreditCard card) throws DaoException {
        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(ADD_CARD);
            ps.setLong(1, card.getCardNumber());
            ps.setLong(2, card.getAccountId());
            ps.setDate(3, card.getExpirationDate());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by adding card to database", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * Deletes cards from database
     *
     * @param cardIds ids of cards for deletion
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public void deleteChosenCards(int[] cardIds) throws DaoException {
        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder(DELETE_CHOSEN_CARDS);
            for (int i = 0; i < cardIds.length; i++) {
                sql.append("?");
                if (i != cardIds.length - 1) {
                    sql.append(",");
                }
            }
            sql.append(");");
            ps = connection.prepareStatement(sql.toString());

            for (int i = 1; i <= cardIds.length; i++) {
                ps.setInt(i, cardIds[i - 1]);
            }

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by deletion chosen cards", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * Gets list of credit cards
     *
     * @param accountId account id which cards we are interested in
     * @return new ArrayList of credit-cards
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public List<CreditCard> listCards(int accountId) throws DaoException {
        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement ps = null;
        List<CreditCard> creditCards = null;
        try {
            ps = connection.prepareStatement(LIST_CARDS);
            ps.setInt(1, accountId);
            ResultSet result = ps.executeQuery();
            creditCards = new ArrayList<>();
            while (result.next()) {
                creditCards.add(setupCardFields(result));
            }
        } catch (SQLException e) {
            throw new DaoException("Error by getting list of cards", e);
        } finally {
            closeResources(connection, ps);
        }
        return creditCards;
    }

    /**
     * Fetches account id by card id
     *
     * @param cardId id of definite card
     * @return id of account that is connected with definite card
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public int fetchAccountIdByCardId(int cardId) throws DaoException {

        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement ps = null;
        int accountId = 0;
        try {
            ps = connection.prepareStatement(FETCH_ACCOUNT_ID_BY_CARD_ID);
            ps.setInt(1, cardId);
            ResultSet result = ps.executeQuery();
            if (result.next()) {
                accountId = result.getInt(ColumnLabels.ACCOUNT_ID);
            }
        } catch (SQLException e) {
            throw new DaoException("Error by fetching account by card-id", e);
        } finally {
            closeResources(connection, ps);
        }
        return accountId;
    }

    /**
     * Sets up fields of CreditCard instance
     *
     * @param result ResultSet instance
     * @return new CreditCard
     * @throws SQLException
     */
    private CreditCard setupCardFields(ResultSet result) throws SQLException {

        CreditCard creditCard = new CreditCard();
        creditCard.setCardId(result.getInt(ColumnLabels.CARD_ID));
        creditCard.setCardNumber(result.getLong(ColumnLabels.CARD_NUMBER));
        creditCard.setAccountId(result.getInt(ColumnLabels.ACCOUNT_ID));
        creditCard.setExpirationDate(new Date(result.getDate(ColumnLabels.EXPIRATION_DATE).getTime()));

        return creditCard;
    }
}