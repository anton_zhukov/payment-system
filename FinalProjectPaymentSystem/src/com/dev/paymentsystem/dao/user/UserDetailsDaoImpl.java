package com.dev.paymentsystem.dao.user;

import com.dev.paymentsystem.constants.ColumnLabels;
import com.dev.paymentsystem.dao.Dao;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.User;
import com.dev.paymentsystem.entity.UserDetails;
import com.dev.paymentsystem.pool.ConnectionPool;
import com.dev.paymentsystem.pool.ConnectionProxy;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDetailsDaoImpl extends Dao implements UserDetailsDao {

    private static final String LIST_USER_DETAILS = "SELECT (SELECT COUNT(account.account_id) FROM account WHERE account.status_id IN(2,3) \n" +
            "AND account.user_id=users.user_id), \n" +
            "users.user_id, users.login, \n" +
            "users.password, users.name, users.surname, users.type, users.email \n" +
            "FROM users\n" +
            "WHERE users.type=0;";

    /**
     * Gets list of UserDetails
     *
     * @return new ArrayList containing details about users
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public List<UserDetails> listUserDetails() throws DaoException {
        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        List<UserDetails> users = null;
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(LIST_USER_DETAILS);
            ResultSet rs = ps.executeQuery();
            users = new ArrayList<>();
            while (rs.next()) {
                users.add(setupUserFields(rs));
            }
        } catch (SQLException e) {
            throw new DaoException("Error by getting list of users", e);
        } finally {
            closeResources(connection, ps);
        }
        return users;
    }

    /**
     * Sets up fields of UserDetails instance
     *
     * @param rs ResultSet instance
     * @return new UserDetails
     * @throws SQLException
     */
    private UserDetails setupUserFields(ResultSet rs) throws SQLException {

        User user = new User();
        user.setUserId(rs.getInt(ColumnLabels.USER_ID));
        user.setLogin(rs.getString(ColumnLabels.LOGIN));
        user.setName(rs.getString(ColumnLabels.NAME));
        user.setSurname(rs.getString(ColumnLabels.SURNAME));
        user.setType(rs.getInt(ColumnLabels.TYPE));
        user.setEmailAddress(rs.getString(ColumnLabels.EMAIL));

        UserDetails userDetails = new UserDetails();
        userDetails.setUser(user);
        userDetails.setNotActiveAccounts(rs.getInt(1));

        return userDetails;
    }
}