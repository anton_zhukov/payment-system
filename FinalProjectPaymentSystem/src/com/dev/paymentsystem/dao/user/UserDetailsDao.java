package com.dev.paymentsystem.dao.user;

import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.UserDetails;

import java.util.List;

public interface UserDetailsDao {

    /**
     * Gets list of UserDetails
     *
     * @return new ArrayList containing details about users
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    List<UserDetails> listUserDetails() throws DaoException;
}
