package com.dev.paymentsystem.dao.user;

import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.User;

public interface UserDao {

    /**
     * Adds new user to database
     *
     * @param user User instance for getting column values
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    void addUser(User user) throws DaoException;

    /**
     * Fetches user if login and pass are correct
     *
     * @param login    input login
     * @param password input hidden password
     * @return User instance
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    User fetchByLoginPassword(String login, String password) throws DaoException;

    /**
     * Fetches user if login is correct
     *
     * @param login input login
     * @return {@code true} if user with such login exists; {@code false} otherwise
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    boolean fetchByLogin(String login) throws DaoException;
}