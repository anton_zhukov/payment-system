package com.dev.paymentsystem.dao.user;

import com.dev.paymentsystem.constants.ColumnLabels;
import com.dev.paymentsystem.dao.Dao;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.User;
import com.dev.paymentsystem.pool.ConnectionPool;
import com.dev.paymentsystem.pool.ConnectionProxy;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDaoImpl extends Dao implements UserDao {

    private static final String ADD_USER = "INSERT INTO users (login, password, name, surname, type, email) " +
            "values (?, ?, ?, ?, 0, ?);";
    private static final String FETCH_BY_LOGIN_PASSWORD = "select user_id, login, password, name, surname, type, email " +
            "from users where BINARY login=? and BINARY password=?;";
    private static final String FETCH_BY_LOGIN = "SELECT COUNT(login) FROM users WHERE login=?;";

    /**
     * Adds new user to database
     *
     * @param user User instance for getting column values
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public void addUser(User user) throws DaoException {
        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(ADD_USER);
            ps.setString(1, user.getLogin());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setString(4, user.getSurname());
            ps.setString(5, user.getEmailAddress());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by adding user in database", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * Fetches user if login and pass are correct
     *
     * @param login    input login
     * @param password input hidden password
     * @return User instance
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public User fetchByLoginPassword(String login, String password) throws DaoException {

        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        User user = null;
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(FETCH_BY_LOGIN_PASSWORD);
            ps.setString(1, login);
            ps.setString(2, password);
            ResultSet result = ps.executeQuery();
            if (result.next()) {
                user = setupUserFields(result);
            }
        } catch (SQLException e) {
            throw new DaoException("Error by fetching user by login and password", e);
        } finally {
            closeResources(connection, ps);
        }
        return user;
    }

    /**
     * Fetches user if login is correct
     *
     * @param login input login
     * @return {@code true} if user with such login exists; {@code false} otherwise
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public boolean fetchByLogin(String login) throws DaoException {
        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        boolean userExists = false;
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(FETCH_BY_LOGIN);
            ps.setString(1, login);
            ResultSet result = ps.executeQuery();
            if (result.next()) {
                userExists = result.getInt(1) > 0;
            }
        } catch (SQLException e) {
            throw new DaoException("Error by fetching user by login", e);
        } finally {
            closeResources(connection, ps);
        }
        return userExists;
    }

    /**
     * Sets up fields of User instance
     *
     * @param result ResultSet instance
     * @return new User
     * @throws SQLException
     */
    private User setupUserFields(ResultSet result) throws SQLException {

        User user = new User();
        user.setUserId(result.getInt(ColumnLabels.USER_ID));
        user.setLogin(result.getString(ColumnLabels.LOGIN));
        user.setName(result.getString(ColumnLabels.NAME));
        user.setSurname(result.getString(ColumnLabels.SURNAME));
        user.setType(result.getInt(ColumnLabels.TYPE));
        user.setEmailAddress(result.getString(ColumnLabels.EMAIL));

        return user;
    }
}