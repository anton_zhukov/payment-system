package com.dev.paymentsystem.dao.account;

import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.AccountDetails;

import java.util.List;

public interface AccountDetailsDao {

    /**
     * Gets list of AccountDetails
     *
     * @param userId id of definite user
     * @return new ArrayList containing details about users' accounts
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    List<AccountDetails> listAccounts(int userId) throws DaoException;

    /**
     * Gets definite account details
     *
     * @param accountId id of definite user's account
     * @return new AccountDetails instance containing details about user's account
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    AccountDetails fetchAccountDetail(int accountId) throws DaoException;
}