package com.dev.paymentsystem.dao.account;

import com.dev.paymentsystem.constants.ColumnLabels;
import com.dev.paymentsystem.dao.Dao;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.*;
import com.dev.paymentsystem.pool.ConnectionPool;
import com.dev.paymentsystem.pool.ConnectionProxy;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AccountDetailsDaoImpl extends Dao implements AccountDetailsDao {

    private static final String LIST_ACCOUNTS_DETAILS = "SELECT acc.account_id, acc.balance, acc.account_number, acc.user_id,\n" +
            "acc.bank_id, bank.bank_name,\n" +
            "acc.currency_id, curr.currency_code, curr.min_amount,\n" +
            "acc.status_id, st.status_value \n" +
            "FROM account AS acc \n" +
            "INNER JOIN bank AS bank\n" +
            "ON acc.bank_id=bank.bank_id\n" +
            "INNER JOIN currency AS curr\n" +
            "ON acc.currency_id=curr.currency_id\n" +
            "INNER JOIN status AS st\n" +
            "ON acc.status_id=st.status_id\n" +
            "WHERE user_id=? \n" +
            "ORDER BY acc.status_id ASC, acc.account_number ASC;";

    private static final String FETCH_ACCOUNT_DETAIL = "SELECT acc.account_id, acc.balance, acc.account_number, acc.user_id,\n" +
            "acc.bank_id, bank.bank_name,\n" +
            "acc.currency_id, curr.currency_code, curr.min_amount,\n" +
            "acc.status_id, st.status_value \n" +
            "FROM account AS acc \n" +
            "INNER JOIN bank AS bank\n" +
            "ON acc.bank_id=bank.bank_id\n" +
            "INNER JOIN currency AS curr\n" +
            "ON acc.currency_id=curr.currency_id\n" +
            "INNER JOIN status AS st\n" +
            "ON acc.status_id=st.status_id\n" +
            "WHERE acc.account_id=? \n" +
            "ORDER BY st.status_value DESC, account_number ASC;";

    /**
     * Gets list of AccountDetails
     *
     * @param userId id of definite user
     * @return new ArrayList containing details about users' accounts
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public List<AccountDetails> listAccounts(int userId) throws DaoException {

        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        List<AccountDetails> accounts = null;
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(LIST_ACCOUNTS_DETAILS);
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            accounts = new ArrayList<>();
            while (rs.next()) {
                accounts.add(setupAccountDetailsFields(rs));
            }
        } catch (SQLException e) {
            throw new DaoException("Error by getting list of accounts", e);
        } finally {
            closeResources(connection, ps);
        }
        return accounts;
    }

    /**
     * Gets definite account details
     *
     * @param accountId id of definite user's account
     * @return new AccountDetails instance containing details about user's account
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public AccountDetails fetchAccountDetail(int accountId) throws DaoException {

        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        AccountDetails accountDetail = null;
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(FETCH_ACCOUNT_DETAIL);
            ps.setInt(1, accountId);
            ResultSet rs = ps.executeQuery();
            accountDetail = new AccountDetails();
            while (rs.next()) {
                accountDetail = setupAccountDetailsFields(rs);
            }
        } catch (SQLException e) {
            throw new DaoException("Error by fetching account details", e);
        } finally {
            closeResources(connection, ps);
        }
        return accountDetail;
    }

    /**
     * Sets up fields of AccountDetails instance
     *
     * @param rs ResultSet instance
     * @return new AccountDetails
     * @throws SQLException
     */
    private AccountDetails setupAccountDetailsFields(ResultSet rs) throws SQLException {
        AccountDetails accountDetails = new AccountDetails();

        Account account = new Account();
        account.setAccountId(rs.getInt(ColumnLabels.ACCOUNT_ID));
        account.setCurrentBalance(rs.getDouble(ColumnLabels.BALANCE));
        account.setBankId(rs.getInt(ColumnLabels.BANK_ID));
        account.setCurrencyId(rs.getInt(ColumnLabels.CURRENCY_ID));
        account.setStatusId(rs.getInt(ColumnLabels.STATUS_ID));
        account.setAccountNumber(rs.getString(ColumnLabels.ACCOUNT_NUMBER));
        account.setUserId(rs.getInt(ColumnLabels.USER_ID));

        Bank bank = new Bank();
        bank.setBankId(rs.getInt(ColumnLabels.BANK_ID));
        bank.setBankName(rs.getString(ColumnLabels.BANK_NAME));

        Currency currency = new Currency();
        currency.setCurrencyId(rs.getInt(ColumnLabels.CURRENCY_ID));
        currency.setCurrencyCode(rs.getString(ColumnLabels.CURRENCY_CODE));
        currency.setMinAmount(rs.getDouble(ColumnLabels.MIN_AMOUNT));

        AccountStatus status = new AccountStatus();
        status.setStatusId(rs.getInt(ColumnLabels.STATUS_ID));
        status.setStatusValue(rs.getString(ColumnLabels.STATUS_VALUE));

        accountDetails.setAccount(account);
        accountDetails.setBank(bank);
        accountDetails.setCurrency(currency);
        accountDetails.setAccountStatus(status);

        return accountDetails;
    }
}