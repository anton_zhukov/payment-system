package com.dev.paymentsystem.dao.account;

import com.dev.paymentsystem.constants.ColumnLabels;
import com.dev.paymentsystem.dao.Dao;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.Account;
import com.dev.paymentsystem.pool.ConnectionPool;
import com.dev.paymentsystem.pool.ConnectionProxy;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AccountDaoImpl extends Dao implements AccountDao {

    private static final String ADD_ACCOUNT = "INSERT INTO account (balance, " +
            "bank_id, currency_id, status_id, account_number, user_id) VALUES (?, ?, ?, 3, ?, ?);";
    private static final String DELETE_ACCOUNT = "DELETE FROM account WHERE account_id=?;";
    private static final String FETCH_CURRENT_BALANCE = "SELECT balance FROM account WHERE account_id=?;";
    private static final String DOES_ACCOUNT_HAVE_CARDS = "SELECT COUNT(card_id) FROM creditcard AS card WHERE account_id=?;";
    private static final String CHANGE_STATUS = "UPDATE account SET status_id=? WHERE account_id=?;";
    private static final String REPLENISH_BALANCE = "UPDATE account SET balance=balance+? WHERE account_id=?;";

    /**
     * Method for adding new account to database
     *
     * @param account Account instance for getting column values
     * @return id of added account
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public int addAccount(Account account) throws DaoException {

        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement ps = null;

        int accountId = 0;
        try {
            ps = connection.prepareStatement(ADD_ACCOUNT, Statement.RETURN_GENERATED_KEYS);
            ps.setDouble(1, account.getCurrentBalance());
            ps.setInt(2, account.getBankId());
            ps.setInt(3, account.getCurrencyId());
            ps.setString(4, account.getAccountNumber());
            ps.setInt(5, account.getUserId());
            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                accountId = rs.getInt(1);
            }
        } catch (SQLException e) {
            throw new DaoException("Error by adding account", e);
        } finally {
            closeResources(connection, ps);
        }
        return accountId;
    }

    /**
     * Method for deletion account from database
     *
     * @param accountId id of definite account
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public void deleteAccount(int accountId) throws DaoException {
        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(DELETE_ACCOUNT);
            ps.setInt(1, accountId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by deletion account", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * Fetches current account balance
     *
     * @param accountId id of definite account
     * @return amount of current balance
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public double fetchCurrentBalance(int accountId) throws DaoException {
        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        double currentBalance = 0;
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(FETCH_CURRENT_BALANCE);
            ps.setInt(1, accountId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                currentBalance = rs.getDouble(ColumnLabels.BALANCE);
            }
        } catch (SQLException e) {
            throw new DaoException("Error by fetching current balance", e);
        } finally {
            closeResources(connection, ps);
        }
        return currentBalance;
    }

    /**
     * Indicates whether account has credit cards or not
     *
     * @param accountId id of definite account
     * @return {@code true} if account has credit cards; {@code false} otherwise.
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public boolean doesAccountHaveCards(int accountId) throws DaoException {

        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement ps = null;
        boolean hasCards = false;
        try {
            ps = connection.prepareStatement(DOES_ACCOUNT_HAVE_CARDS);
            ps.setDouble(1, accountId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                hasCards = rs.getInt(1) > 0;
            }
        } catch (SQLException e) {
            throw new DaoException("Error by checking: does account have cards", e);
        } finally {
            closeResources(connection, ps);
        }
        return hasCards;
    }

    /**
     * Changes account status
     *
     * @param statusId  id of required account status
     * @param accountId id of definite account
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public void changeAccountStatus(int statusId, int accountId) throws DaoException {

        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(CHANGE_STATUS);
            ps.setInt(1, statusId);
            ps.setInt(2, accountId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by changing account status", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * Increases account balance
     *
     * @param accountId id of definite account
     * @param amount    replenishment's amount
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    @Override
    public void replenishBalance(int accountId, double amount) throws DaoException {

        ConnectionProxy connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(REPLENISH_BALANCE);
            ps.setDouble(1, amount);
            ps.setInt(2, accountId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by replenishing balance", e);
        } finally {
            closeResources(connection, ps);
        }
    }

}