package com.dev.paymentsystem.dao.account;

import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.Account;

public interface AccountDao {

    /**
     * Method for adding new account to database
     *
     * @param account Account instance for getting column values
     * @return id of added account
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    int addAccount(Account account) throws DaoException;

    /**
     * Method for deletion account from database
     *
     * @param accountId id of definite account
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    void deleteAccount(int accountId) throws DaoException;

    /**
     * Fetches current account balance
     *
     * @param accountId id of definite account
     * @return amount of current balance
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    double fetchCurrentBalance(int accountId) throws DaoException;

    /**
     * Indicates whether account has credit cards or not
     *
     * @param accountId id of definite account
     * @return {@code true} if account has credit cards; {@code false} otherwise
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    boolean doesAccountHaveCards(int accountId) throws DaoException;

    /**
     * Changes account status
     *
     * @param statusId  id of required account status
     * @param accountId id of definite account
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    void changeAccountStatus(int statusId, int accountId) throws DaoException;

    /**
     * Increases account balance
     *
     * @param accountId id of definite account
     * @param amount    replenishment's amount
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    void replenishBalance(int accountId, double amount) throws DaoException;

}