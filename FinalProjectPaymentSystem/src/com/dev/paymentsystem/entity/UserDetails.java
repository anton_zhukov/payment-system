package com.dev.paymentsystem.entity;


public class UserDetails {

    private User user;
    private int notActiveAccounts;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getNotActiveAccounts() {
        return notActiveAccounts;
    }

    public void setNotActiveAccounts(int notActiveAccounts) {
        this.notActiveAccounts = notActiveAccounts;
    }
}
