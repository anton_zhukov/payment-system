package com.dev.paymentsystem.entity;

import java.util.LinkedList;
import java.util.List;

public class ErrorMessage {

    private String key;
    private List<String> values;

    public ErrorMessage(String key) {
        this.key = key;
        values = new LinkedList<>();
    }

    public ErrorMessage(String key, List<String> value) {
        this.key = key;
        this.values = value;
    }

    public String getKey() {
        return key;
    }

    public List<String> getValues() {
        return values;
    }
}