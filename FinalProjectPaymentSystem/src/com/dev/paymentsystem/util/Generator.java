package com.dev.paymentsystem.util;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Generator {

    private static final int CARD_NUMBER_LENGTH = 16;
    private static final int ACCOUNT_NUMBERS_PART_LENGTH = 6;

    /**
     * Generates expiration date of created credit-card
     *
     * @return new java.sql.Date
     */
    public static Date generateExpirationDate() {
        Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.YEAR, +3);
        return new Date(calendar.getTime().getTime());
    }

    /**
     * Generates time and date of added payment
     *
     * @return new java.sql.Timestamp
     */
    public static Timestamp generatePaymentDate() {
        Calendar calendar = new GregorianCalendar();
        return new Timestamp(calendar.getTime().getTime());
    }

    /**
     * Generates credit-card number
     *
     * @return new credit-card number
     */
    public static long generateCardNumber() {
        return generateNumber(CARD_NUMBER_LENGTH);
    }

    /**
     * Generates account number
     *
     * @return new account number
     */
    public static String generateAccountNumber() {
        char randomChar = (char) ((int) 'A' + Math.random() * ((int) 'Z' - (int) 'A' + 1));
        long number = generateNumber(ACCOUNT_NUMBERS_PART_LENGTH);
        return String.valueOf(randomChar) + number;
    }

    /**
     * Generates number with set length
     *
     * @param numberLength length of required number
     * @return number with definite length
     */
    private static long generateNumber(int numberLength) {
        return (long) ((Math.random() + 1) * Math.pow(10, numberLength - 1));
    }
}