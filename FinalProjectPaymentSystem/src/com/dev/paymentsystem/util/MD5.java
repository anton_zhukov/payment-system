package com.dev.paymentsystem.util;

import org.apache.log4j.Logger;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Util for hashing input password
 */
public class MD5 {

    private static final Logger LOG = Logger.getLogger(MD5.class);

    private static final String ALGORITHM = "MD5";

    /**
     * Hashes password of user
     *
     * @param password password for hashing
     * @return hashed view of password
     */
    public static String hidePass(String password) {

        MessageDigest md;
        String md5String = null;
        try {
            md = MessageDigest.getInstance(ALGORITHM);
            md.update(password.getBytes());
            byte byteData[] = md.digest();

            StringBuilder sb = new StringBuilder();
            for (byte data : byteData) {
                sb.append(Integer.toString((data & 0xff) + 0x100, 16).substring(1));
            }
            md5String = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            LOG.fatal("Error with hashing algorithm", e);
            throw new RuntimeException();
        }
        return md5String;
    }
}