package com.dev.paymentsystem.command;

import com.dev.paymentsystem.command.account.*;
import com.dev.paymentsystem.command.bank.AddBankCommand;
import com.dev.paymentsystem.command.bank.ListBanksCommand;
import com.dev.paymentsystem.command.bank.ViewAddBankCommand;
import com.dev.paymentsystem.command.card.AddCardCommand;
import com.dev.paymentsystem.command.card.DeleteCardCommand;
import com.dev.paymentsystem.command.card.ListCreditCardsCommand;
import com.dev.paymentsystem.command.currency.AddCurrencyCommand;
import com.dev.paymentsystem.command.currency.ListCurrenciesCommand;
import com.dev.paymentsystem.command.currency.ViewAddCurrencyCommand;
import com.dev.paymentsystem.command.exception.CommandException;
import com.dev.paymentsystem.command.payment.AddPaymentCommand;
import com.dev.paymentsystem.command.payment.ListPaymentsByAccountCommand;
import com.dev.paymentsystem.command.payment.ListPaymentsByCardCommand;
import com.dev.paymentsystem.command.payment.ViewAddPaymentCommand;
import com.dev.paymentsystem.command.user.*;
import com.dev.paymentsystem.constants.CommandKeys;
import com.dev.paymentsystem.constants.Parameters;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

public class CommandHolder {

    private static CommandHolder instance = new CommandHolder();
    private HashMap<String, Command> commands = new HashMap<>();

    private CommandHolder() {

        commands.put(CommandKeys.USER_LOGIN, new UserLoginCommand());
        commands.put(CommandKeys.USER_LOGOUT, new UserLogoutCommand());
        commands.put(CommandKeys.REGISTRATE_USER, new AddUserCommand());
        commands.put(CommandKeys.VIEW_REGISTRATION, new ViewRegistrationCommand());
        commands.put(CommandKeys.VIEW_LOGIN, new ViewLoginCommand());
        commands.put(CommandKeys.VIEW_ADMIN, new ViewAdminPageCommand());
        commands.put(CommandKeys.LIST_USERS, new ListUserDetailsCommand());
        commands.put(CommandKeys.VIEW_POST_REGISTRATION, new ViewPostRegistrationCommand());

        commands.put(CommandKeys.LIST_BANKS, new ListBanksCommand());
        commands.put(CommandKeys.ADD_BANK, new AddBankCommand());
        commands.put(CommandKeys.VIEW_ADD_BANK, new ViewAddBankCommand());

        commands.put(CommandKeys.LIST_CURRENCIES, new ListCurrenciesCommand());
        commands.put(CommandKeys.ADD_CURRENCY, new AddCurrencyCommand());
        commands.put(CommandKeys.VIEW_ADD_CURRENCY, new ViewAddCurrencyCommand());

        commands.put(CommandKeys.CHANGE_LOCALE, new ChangeLocaleCommand());

        commands.put(CommandKeys.ADD_CARD, new AddCardCommand());
        commands.put(CommandKeys.DELETE_CARD, new DeleteCardCommand());
        commands.put(CommandKeys.LIST_CARDS, new ListCreditCardsCommand());

        commands.put(CommandKeys.LIST_PAYMENTS_BY_CARD, new ListPaymentsByCardCommand());
        commands.put(CommandKeys.LIST_PAYMENTS_BY_ACCOUNT, new ListPaymentsByAccountCommand());
        commands.put(CommandKeys.ADD_PAYMENT, new AddPaymentCommand());
        commands.put(CommandKeys.VIEW_ADD_PAYMENT, new ViewAddPaymentCommand());

        commands.put(CommandKeys.ADD_ACCOUNT, new AddAccountCommand());
        commands.put(CommandKeys.LIST_ACCOUNTS, new ListAccountsCommand());
        commands.put(CommandKeys.ACTIVATE_ACCOUNT, new ActivateAccountCommand());
        commands.put(CommandKeys.BLOCK_ACCOUNT, new BlockAccountCommand());
        commands.put(CommandKeys.DELETE_ACCOUNT, new DeleteAccountCommand());
        commands.put(CommandKeys.VIEW_ADD_ACCOUNT_FIELDS, new ViewAddAccountCommand());
        commands.put(CommandKeys.REPLENISH_BALANCE, new ReplenishBalanceCommand());
        commands.put(CommandKeys.VIEW_REPLENISH_BALANCE_PAGE, new ViewReplenishBalanceCommand());

    }

    /**
     * Gets CommandHolder instance
     *
     * @return CommandHolder instance
     */
    public static CommandHolder getInstance() {
        return instance;
    }

    /**
     * Defines command for execution
     *
     * @param request HttpServletRequest instance
     * @return definite implementation of interface Command
     */
    public Command getCommand(HttpServletRequest request) throws CommandException {

        String commandName = request.getParameter(Parameters.ACTION);

        if (!commands.containsKey(commandName)) {
            throw new CommandException(String.format("No such command: " + commandName));
        }

        return commands.get(commandName);
    }
}