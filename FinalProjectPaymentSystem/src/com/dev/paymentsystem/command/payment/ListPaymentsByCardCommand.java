package com.dev.paymentsystem.command.payment;

import com.dev.paymentsystem.command.Command;
import com.dev.paymentsystem.command.exception.CommandException;
import com.dev.paymentsystem.constants.Attributes;
import com.dev.paymentsystem.constants.Pages;
import com.dev.paymentsystem.constants.Parameters;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.ForwardAction;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.dao.payment.PaymentDetailsDao;
import com.dev.paymentsystem.dao.payment.PaymentDetailsDaoImpl;
import com.dev.paymentsystem.entity.PaymentDetails;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ListPaymentsByCardCommand implements Command {

    /**
     * Lists payments on page by card id
     *
     * @param request HttpServletRequest instance
     * @return new ForwardAction instance
     * @throws com.dev.paymentsystem.command.exception.CommandException
     */
    @Override
    public Action execute(HttpServletRequest request) throws CommandException {

        int cardId = Integer.parseInt(request.getParameter(Parameters.CARD_ID));
        PaymentDetailsDao paymentDetailsDao = new PaymentDetailsDaoImpl();
        List<PaymentDetails> payments = null;
        try {
            payments = paymentDetailsDao.listPaymentsByCard(cardId);
            request.setAttribute(Attributes.PAYMENTS, payments);
        } catch (DaoException e) {
            throw new CommandException("Error by getting list of payments by card", e);
        }

        return new ForwardAction(Pages.LIST_PAYMENTS);
    }
}