package com.dev.paymentsystem.command.payment;

import com.dev.paymentsystem.command.Command;
import com.dev.paymentsystem.command.exception.CommandException;
import com.dev.paymentsystem.constants.Attributes;
import com.dev.paymentsystem.constants.Parameters;
import com.dev.paymentsystem.constants.Paths;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.ForwardAction;
import com.dev.paymentsystem.controller.action.RedirectAction;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.dao.payment.PaymentDao;
import com.dev.paymentsystem.dao.payment.PaymentDaoImpl;
import com.dev.paymentsystem.entity.Payment;
import com.dev.paymentsystem.util.Generator;
import com.dev.paymentsystem.validator.ErrorHolder;
import com.dev.paymentsystem.validator.PaymentValidator;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.concurrent.locks.ReentrantLock;

public class AddPaymentCommand implements Command {

    private static final ReentrantLock LOCK = new ReentrantLock();

    /**
     * Adds new payment
     *
     * @param request HttpServletRequest instance
     * @return new RedirectAction in case of no errors, new ForwardAction otherwise
     * @throws com.dev.paymentsystem.command.exception.CommandException
     */
    @Override
    public Action execute(HttpServletRequest request) throws CommandException {

        ErrorHolder holder = null;
        String path = null;
        Action action = null;

        try {
            LOCK.lock();
            Payment payment = parse(request);
            Timestamp paymentDate = Generator.generatePaymentDate();
            payment.setPaymentDate(paymentDate);

            int accountId = Integer.parseInt(request.getParameter(Parameters.ACCOUNT_ID));

            try {
                holder = PaymentValidator.validatePayment(payment, accountId);
                if (!holder.haveErrors()) {
                    PaymentDao paymentDao = new PaymentDaoImpl();
                    paymentDao.addPayment(payment, accountId);
                    path = String.format(Paths.LIST_PAYMENTS_BY_CARD, payment.getCardId(), accountId);
                    action = new RedirectAction(path);
                } else {
                    request.setAttribute(Attributes.ERROR_HOLDER, holder);
                    request.setAttribute(Attributes.PAYMENT, payment);
                    path = String.format(Paths.VIEW_ADD_PAYMENT, payment.getCardId(), accountId);
                    action = new ForwardAction(path);
                }
            } catch (DaoException e) {
                throw new CommandException("Error by adding payment", e);
            }
        } finally {
            LOCK.unlock();
        }

        return action;
    }

    /**
     * Parses data for getting Payment instance
     *
     * @param request HttpServletRequest instance
     * @return new Payment instance
     */
    private Payment parse(HttpServletRequest request) {

        double amount = Double.parseDouble(request.getParameter(Parameters.AMOUNT));
        int cardId = Integer.parseInt(request.getParameter(Parameters.CARD_ID));
        String receiverAccount = request.getParameter(Parameters.RECEIVER_ACCOUNT);

        Payment payment = new Payment();
        payment.setAmount(amount);
        payment.setCardId(cardId);
        payment.setReceiverAccount(receiverAccount);

        return payment;
    }
}