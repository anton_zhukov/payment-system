package com.dev.paymentsystem.command.payment;

import com.dev.paymentsystem.command.Command;
import com.dev.paymentsystem.command.exception.CommandException;
import com.dev.paymentsystem.constants.Attributes;
import com.dev.paymentsystem.constants.Pages;
import com.dev.paymentsystem.constants.Parameters;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.ForwardAction;
import com.dev.paymentsystem.dao.card.CreditCardDaoImpl;
import com.dev.paymentsystem.dao.exception.DaoException;

import javax.servlet.http.HttpServletRequest;

public class ViewAddPaymentCommand implements Command {

    /**
     * Forwards to add payment page with account id for definite credit-card
     *
     * @param request HttpServletRequest instance
     * @return new ForwardAction instance
     * @throws com.dev.paymentsystem.command.exception.CommandException
     */
    @Override
    public Action execute(HttpServletRequest request) throws CommandException {

        int cardId = Integer.parseInt(request.getParameter(Parameters.CARD_ID));
        CreditCardDaoImpl cardDao = new CreditCardDaoImpl();
        try {
            int accountId = cardDao.fetchAccountIdByCardId(cardId);
            request.setAttribute(Attributes.ACCOUNT_ID, accountId);
        } catch (DaoException e) {
            throw new CommandException("Error by viewing add payment page", e);
        }

        return new ForwardAction(Pages.ADD_PAYMENT_PAGE);
    }
}