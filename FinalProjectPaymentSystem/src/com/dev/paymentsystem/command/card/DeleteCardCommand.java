package com.dev.paymentsystem.command.card;

import com.dev.paymentsystem.command.Command;
import com.dev.paymentsystem.command.exception.CommandException;
import com.dev.paymentsystem.constants.Paths;
import com.dev.paymentsystem.constants.Attributes;
import com.dev.paymentsystem.constants.Parameters;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.ForwardAction;
import com.dev.paymentsystem.controller.action.RedirectAction;
import com.dev.paymentsystem.dao.card.CreditCardDao;
import com.dev.paymentsystem.dao.card.CreditCardDaoImpl;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.validator.ErrorHolder;

import javax.servlet.http.HttpServletRequest;

public class DeleteCardCommand implements Command {

    private static final String NO_CARDS_SELECTED_ERROR = "cards.error.no-cards-selected";

    /**
     * Deletes credit-card
     *
     * @param request HttpServletRequest instance
     * @return new RedirectAction in case of no errors, new ForwardAction otherwise
     * @throws com.dev.paymentsystem.command.exception.CommandException
     */
    @Override
    public Action execute(HttpServletRequest request) throws CommandException {

        int accountId = Integer.parseInt(request.getParameter(Parameters.ACCOUNT_ID));
        String path = String.format(Paths.LIST_CARDS, accountId);
        String select[] = request.getParameterValues(Parameters.CARDS_IDS);
        Action action = null;

        if (select != null) {
            try {
                int[] cardIds = new int[select.length];
                for (int i = 0; i < cardIds.length; i++) {
                    cardIds[i] = Integer.parseInt(select[i]);
                }

                CreditCardDao dao = new CreditCardDaoImpl();
                dao.deleteChosenCards(cardIds);
                action = new RedirectAction(path);
            } catch (DaoException e) {
                throw new CommandException("Error by deletion credit-card", e);
            }
        } else {
            ErrorHolder holder = new ErrorHolder();
            holder.addError(NO_CARDS_SELECTED_ERROR);
            request.setAttribute(Attributes.ERROR_HOLDER, holder);
            action = new ForwardAction(path);
        }

        return action;
    }
}