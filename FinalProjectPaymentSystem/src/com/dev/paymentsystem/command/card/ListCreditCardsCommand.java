package com.dev.paymentsystem.command.card;

import com.dev.paymentsystem.command.Command;
import com.dev.paymentsystem.command.exception.CommandException;
import com.dev.paymentsystem.constants.Attributes;
import com.dev.paymentsystem.constants.Pages;
import com.dev.paymentsystem.constants.Parameters;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.ForwardAction;
import com.dev.paymentsystem.dao.account.AccountDetailsDao;
import com.dev.paymentsystem.dao.account.AccountDetailsDaoImpl;
import com.dev.paymentsystem.dao.card.CreditCardDao;
import com.dev.paymentsystem.dao.card.CreditCardDaoImpl;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.AccountDetails;
import com.dev.paymentsystem.entity.CreditCard;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ListCreditCardsCommand implements Command {

    /**
     * Lists credit-cards on page
     *
     * @param request HttpServletRequest instance
     * @return new ForwardAction instance
     * @throws com.dev.paymentsystem.command.exception.CommandException
     */
    @Override
    public Action execute(HttpServletRequest request) throws CommandException {

        int accountId = Integer.parseInt(request.getParameter(Parameters.ACCOUNT_ID));

        CreditCardDao cardDao = new CreditCardDaoImpl();
        AccountDetailsDao accountDetailsDao = new AccountDetailsDaoImpl();
        List<CreditCard> creditCards = null;
        AccountDetails accountDetails = null;

        try {
            creditCards = cardDao.listCards(accountId);
            accountDetails = accountDetailsDao.fetchAccountDetail(accountId);
            if (creditCards != null) {
                request.setAttribute(Attributes.CARDS, creditCards);
                request.setAttribute(Attributes.ACCOUNT_DETAILS, accountDetails);
            }
        } catch (DaoException e) {
            throw new CommandException("Error by getting list of credit-cards", e);
        }

        return new ForwardAction(Pages.LIST_CARDS_PAGE);
    }
}