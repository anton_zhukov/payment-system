package com.dev.paymentsystem.command.card;

import com.dev.paymentsystem.command.Command;
import com.dev.paymentsystem.command.exception.CommandException;
import com.dev.paymentsystem.constants.Parameters;
import com.dev.paymentsystem.constants.Paths;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.RedirectAction;
import com.dev.paymentsystem.dao.card.CreditCardDao;
import com.dev.paymentsystem.dao.card.CreditCardDaoImpl;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.CreditCard;
import com.dev.paymentsystem.util.Generator;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;

public class AddCardCommand implements Command {

    /**
     * Adds new card
     *
     * @param request HttpServletRequest instance
     * @return new RedirectAction instance
     * @throws com.dev.paymentsystem.command.exception.CommandException
     */
    @Override
    public Action execute(HttpServletRequest request) throws CommandException {

        CreditCard creditCard = parse(request);
        long cardNumber = Generator.generateCardNumber();
        Date expirationDate = Generator.generateExpirationDate();
        creditCard.setCardNumber(cardNumber);
        creditCard.setExpirationDate(expirationDate);

        CreditCardDao cardDao = new CreditCardDaoImpl();
        String path = null;
        try {
            cardDao.addCard(creditCard);
            path = String.format(Paths.LIST_CARDS, creditCard.getAccountId());
        } catch (DaoException e) {
            throw new CommandException("Error by adding credit-card", e);
        }
        return new RedirectAction(path);
    }

    /**
     * Parses data for getting CreditCard instance
     *
     * @param request HttpServletRequest instance
     * @return new CreditCard instance
     */
    private CreditCard parse(HttpServletRequest request) {
        int accountId = Integer.parseInt(request.getParameter(Parameters.ACCOUNT_ID));

        CreditCard creditCard = new CreditCard();
        creditCard.setAccountId(accountId);

        return creditCard;
    }
}