package com.dev.paymentsystem.command;

import com.dev.paymentsystem.command.exception.CommandException;
import com.dev.paymentsystem.controller.action.Action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Command {

    /**
     * @param request HttpServletRequest instance
     * @return child instance of abstract class Action
     * @throws com.dev.paymentsystem.command.exception.CommandException
     */
    Action execute(HttpServletRequest request) throws CommandException;
}