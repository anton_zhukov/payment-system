package com.dev.paymentsystem.command;

import com.dev.paymentsystem.constants.Attributes;
import com.dev.paymentsystem.constants.Pages;
import com.dev.paymentsystem.constants.Parameters;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.RedirectAction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ChangeLocaleCommand implements Command {

    /**
     * Changes current locale
     *
     * @param request HttpServletRequest instance
     * @return new RedirectAction instance
     */
    @Override
    public Action execute(HttpServletRequest request) {

        String locale = request.getParameter(Parameters.LOCALE);

        HttpSession session = request.getSession();
        session.setAttribute(Attributes.USER_LOCALE, locale);

        return new RedirectAction(Pages.HOME_PAGE);
    }
}