package com.dev.paymentsystem.command.currency;

import com.dev.paymentsystem.command.Command;
import com.dev.paymentsystem.command.exception.CommandException;
import com.dev.paymentsystem.constants.Attributes;
import com.dev.paymentsystem.constants.Pages;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.ForwardAction;
import com.dev.paymentsystem.dao.currency.CurrencyDaoImpl;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.Currency;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ListCurrenciesCommand implements Command {

    /**
     * Lists currencies on page
     *
     * @param request HttpServletRequest instance
     * @return new ForwardAction instance
     * @throws com.dev.paymentsystem.command.exception.CommandException
     */
    @Override
    public Action execute(HttpServletRequest request) throws CommandException {

        CurrencyDaoImpl currencyDao = new CurrencyDaoImpl();
        List<Currency> currencies = null;
        try {
            currencies = currencyDao.listCurrencies();
            request.setAttribute(Attributes.CURRENCIES, currencies);
        } catch (DaoException e) {
            throw new CommandException("Error by getting list of currencies", e);
        }

        return new ForwardAction(Pages.CURRENCIES_PAGE);
    }
}