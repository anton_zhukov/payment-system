package com.dev.paymentsystem.command.currency;

import com.dev.paymentsystem.command.Command;
import com.dev.paymentsystem.command.exception.CommandException;
import com.dev.paymentsystem.constants.Attributes;
import com.dev.paymentsystem.constants.Pages;
import com.dev.paymentsystem.constants.Parameters;
import com.dev.paymentsystem.constants.Paths;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.ForwardAction;
import com.dev.paymentsystem.controller.action.RedirectAction;
import com.dev.paymentsystem.dao.currency.CurrencyDao;
import com.dev.paymentsystem.dao.currency.CurrencyDaoImpl;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.Currency;
import com.dev.paymentsystem.validator.CurrencyValidator;
import com.dev.paymentsystem.validator.ErrorHolder;

import javax.servlet.http.HttpServletRequest;

public class AddCurrencyCommand implements Command {

    /**
     * Adds new currency
     *
     * @param request HttpServletRequest instance
     * @return new RedirectAction in case of no errors, new ForwardAction otherwise
     * @throws com.dev.paymentsystem.command.exception.CommandException
     */
    @Override
    public Action execute(HttpServletRequest request) throws CommandException {

        Currency currency = parse(request);
        CurrencyDao currencyDao = new CurrencyDaoImpl();
        Action action = null;
        ErrorHolder holder = null;
        String path = null;
        try {
            holder = CurrencyValidator.validateCurrency(currency);
            if (!holder.haveErrors()) {
                currencyDao.addCurrency(currency);
                path = Paths.LIST_CURRENCIES;
                action = new RedirectAction(path);
            } else {
                request.setAttribute(Attributes.ERROR_HOLDER, holder);
                request.setAttribute(Attributes.CURRENCY, currency);
                path = Pages.ADD_CURRENCY_PAGE;
                action = new ForwardAction(path);
            }
        } catch (DaoException e) {
            throw new CommandException("Error by adding currency", e);
        }
        return action;
    }

    /**
     * Parses data for getting Currency instance
     *
     * @param request HttpServletRequest instance
     * @return new Currency instance
     */
    private Currency parse(HttpServletRequest request) {
        Currency currency = new Currency();
        currency.setCurrencyCode(request.getParameter(Parameters.CURRENCY_CODE));
        currency.setMinAmount(Double.parseDouble(request.getParameter(Parameters.MIN_AMOUNT)));
        return currency;
    }
}