package com.dev.paymentsystem.command.account;

import com.dev.paymentsystem.command.Command;
import com.dev.paymentsystem.command.exception.CommandException;
import com.dev.paymentsystem.constants.Attributes;
import com.dev.paymentsystem.constants.Pages;
import com.dev.paymentsystem.constants.Parameters;
import com.dev.paymentsystem.constants.UserTypes;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.ForwardAction;
import com.dev.paymentsystem.dao.account.AccountDetailsDao;
import com.dev.paymentsystem.dao.account.AccountDetailsDaoImpl;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.AccountDetails;
import com.dev.paymentsystem.entity.User;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ListAccountsCommand implements Command {

    /**
     * Lists accounts on page
     *
     * @param request HttpServletRequest instance
     * @return new ForwardAction instance
     * @throws com.dev.paymentsystem.command.exception.CommandException
     */
    @Override
    public Action execute(HttpServletRequest request) throws CommandException {

        int userId = 0;
        switch (((User) request.getSession().getAttribute(Attributes.USER)).getType()) {
            case UserTypes.CLIENT:
                userId = ((User) request.getSession().getAttribute(Attributes.USER)).getUserId();
                break;
            case UserTypes.ADMIN:
                userId = Integer.parseInt(request.getParameter(Parameters.USER_ID));
                break;
        }

        AccountDetailsDao detailsDao = new AccountDetailsDaoImpl();
        List<AccountDetails> detailsList = null;
        try {
            detailsList = detailsDao.listAccounts(userId);
            request.setAttribute(Attributes.ACCOUNTS, detailsList);
        } catch (DaoException e) {
            throw new CommandException("Error by getting accounts list", e);
        }

        return new ForwardAction(Pages.LIST_ACCOUNTS_PAGE);
    }
}