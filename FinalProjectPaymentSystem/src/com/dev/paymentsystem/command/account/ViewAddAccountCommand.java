package com.dev.paymentsystem.command.account;

import com.dev.paymentsystem.command.Command;
import com.dev.paymentsystem.command.exception.CommandException;
import com.dev.paymentsystem.constants.Attributes;
import com.dev.paymentsystem.constants.Pages;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.ForwardAction;
import com.dev.paymentsystem.dao.bank.BankDao;
import com.dev.paymentsystem.dao.bank.BankDaoImpl;
import com.dev.paymentsystem.dao.currency.CurrencyDao;
import com.dev.paymentsystem.dao.currency.CurrencyDaoImpl;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.Bank;
import com.dev.paymentsystem.entity.Currency;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ViewAddAccountCommand implements Command {

    /**
     * Forwards to add account page with lists of banks and currencies
     *
     * @param request HttpServletRequest instance
     * @return new ForwardAction instance
     * @throws com.dev.paymentsystem.command.exception.CommandException
     */
    @Override
    public Action execute(HttpServletRequest request) throws CommandException {

        BankDao bankDao = new BankDaoImpl();
        CurrencyDao currencyDao = new CurrencyDaoImpl();

        List<Bank> bankList = null;
        List<Currency> currencyList = null;
        try {
            bankList = bankDao.listBanks();
            currencyList = currencyDao.listCurrencies();
            if (bankList != null && currencyList != null) {
                request.setAttribute(Attributes.BANKS, bankList);
                request.setAttribute(Attributes.CURRENCIES, currencyList);
            }
        } catch (DaoException e) {
            throw new CommandException("Error by viewing add account page", e);
        }

        return new ForwardAction(Pages.ADD_ACCOUNT_PAGE);
    }
}