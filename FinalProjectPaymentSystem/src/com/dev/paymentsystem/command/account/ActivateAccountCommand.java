package com.dev.paymentsystem.command.account;

import com.dev.paymentsystem.command.Command;
import com.dev.paymentsystem.command.exception.CommandException;
import com.dev.paymentsystem.constants.AccountStatus;
import com.dev.paymentsystem.constants.Parameters;
import com.dev.paymentsystem.constants.Paths;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.RedirectAction;
import com.dev.paymentsystem.dao.account.AccountDao;
import com.dev.paymentsystem.dao.account.AccountDaoImpl;
import com.dev.paymentsystem.dao.exception.DaoException;

import javax.servlet.http.HttpServletRequest;

public class ActivateAccountCommand implements Command {

    /**
     * Sets active status for account
     *
     * @param request HttpServletRequest instance
     * @return new RedirectAction instance
     * @throws com.dev.paymentsystem.command.Command
     */
    @Override
    public Action execute(HttpServletRequest request) throws CommandException {

        int accountId = Integer.parseInt(request.getParameter(Parameters.ACCOUNT_ID));
        AccountDao dao = new AccountDaoImpl();
        String path = null;
        try {
            dao.changeAccountStatus(AccountStatus.ACTIVE, accountId);
            path = String.format(Paths.LIST_CARDS, accountId);
        } catch (DaoException e) {
            throw new CommandException("Error by activating account", e);
        }

        return new RedirectAction(path);
    }
}