package com.dev.paymentsystem.command.account;

import com.dev.paymentsystem.command.Command;
import com.dev.paymentsystem.command.exception.CommandException;
import com.dev.paymentsystem.constants.Attributes;
import com.dev.paymentsystem.constants.Parameters;
import com.dev.paymentsystem.constants.Paths;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.ForwardAction;
import com.dev.paymentsystem.controller.action.RedirectAction;
import com.dev.paymentsystem.dao.account.AccountDao;
import com.dev.paymentsystem.dao.account.AccountDaoImpl;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.Account;
import com.dev.paymentsystem.validator.ErrorHolder;
import com.dev.paymentsystem.entity.User;
import com.dev.paymentsystem.util.Generator;
import com.dev.paymentsystem.validator.AccountValidator;

import javax.servlet.http.HttpServletRequest;

public class AddAccountCommand implements Command {

    /**
     * Adds new account
     *
     * @param request HttpServletRequest instance
     * @return new RedirectAction in case of no errors, new ForwardAction otherwise
     * @throws com.dev.paymentsystem.command.exception.CommandException
     */
    @Override
    public Action execute(HttpServletRequest request) throws CommandException {

        Account account = parse(request);
        String accountNumber = Generator.generateAccountNumber();
        account.setAccountNumber(accountNumber);

        AccountDao accountDao = new AccountDaoImpl();

        Action action = null;
        ErrorHolder holder = null;
        String path = null;
        try {
            holder = AccountValidator.validateAccount(account);
            if (!holder.haveErrors()) {
                int accountId = accountDao.addAccount(account);
                path = String.format(Paths.LIST_CARDS, accountId);
                action = new RedirectAction(path);
            } else {
                request.setAttribute(Attributes.ERROR_HOLDER, holder);
                request.setAttribute(Attributes.ACCOUNT, account);
                path = Paths.VIEW_ADD_ACCOUNT;
                action = new ForwardAction(path);
            }
        } catch (DaoException e) {
            throw new CommandException("Error by adding account", e);
        }
        return action;
    }

    /**
     * Parses data for getting Account instance
     *
     * @param request HttpServletRequest instance
     * @return new Account instance
     */
    private Account parse(HttpServletRequest request) {

        double beginAmount = Double.parseDouble(request.getParameter(Parameters.BALANCE));
        int bankId = Integer.parseInt(request.getParameter(Parameters.BANK));
        int currencyId = Integer.parseInt(request.getParameter(Parameters.CURRENCY));
        int userId = ((User) request.getSession().getAttribute(Attributes.USER)).getUserId();

        Account account = new Account();
        account.setCurrentBalance(beginAmount);
        account.setBankId(bankId);
        account.setCurrencyId(currencyId);
        account.setUserId(userId);

        return account;
    }
}