package com.dev.paymentsystem.command.account;

import com.dev.paymentsystem.command.Command;
import com.dev.paymentsystem.command.exception.CommandException;
import com.dev.paymentsystem.constants.Attributes;
import com.dev.paymentsystem.constants.Pages;
import com.dev.paymentsystem.constants.Parameters;
import com.dev.paymentsystem.constants.Paths;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.ForwardAction;
import com.dev.paymentsystem.controller.action.RedirectAction;
import com.dev.paymentsystem.dao.account.AccountDao;
import com.dev.paymentsystem.dao.account.AccountDaoImpl;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.validator.ErrorHolder;
import com.dev.paymentsystem.validator.AccountValidator;

import javax.servlet.http.HttpServletRequest;

public class ReplenishBalanceCommand implements Command {

    /**
     * Replenishes balance of account
     *
     * @param request HttpServletRequest instance
     * @return new RedirectAction instance
     * @throws com.dev.paymentsystem.command.exception.CommandException
     */
    @Override
    public Action execute(HttpServletRequest request) throws CommandException {

        AccountDao dao = new AccountDaoImpl();
        int accountId = Integer.parseInt(request.getParameter(Parameters.ACCOUNT_ID));
        double replenishAmount = Double.parseDouble(request.getParameter(Parameters.REPLENISH_AMOUNT));
        String path = null;
        ErrorHolder holder = null;
        Action action = null;
        try {
            holder = AccountValidator.validateReplenishAmount(replenishAmount);
            if (!holder.haveErrors()) {
                dao.replenishBalance(accountId, replenishAmount);
                path = String.format(Paths.LIST_CARDS, accountId);
                action = new RedirectAction(path);
            } else {
                request.setAttribute(Attributes.ERROR_HOLDER, holder);
                request.setAttribute(Attributes.REPLENISH_AMOUNT, replenishAmount);
                path = Pages.REPLENISH_BALANCE_PAGE;
                action = new ForwardAction(path);
            }
        } catch (DaoException e) {
            throw new CommandException("Error by replenishing balance");
        }

        return action;
    }
}