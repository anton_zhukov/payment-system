package com.dev.paymentsystem.command.account;

import com.dev.paymentsystem.command.Command;
import com.dev.paymentsystem.constants.Pages;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.ForwardAction;

import javax.servlet.http.HttpServletRequest;

public class ViewReplenishBalanceCommand implements Command {

    /**
     * Forwards to replenish balance page
     *
     * @param request HttpServletRequest instance
     * @return new ForwardAction instance
     */
    @Override
    public Action execute(HttpServletRequest request) {

        return new ForwardAction(Pages.REPLENISH_BALANCE_PAGE);
    }
}