package com.dev.paymentsystem.command.account;

import com.dev.paymentsystem.command.Command;
import com.dev.paymentsystem.command.exception.CommandException;
import com.dev.paymentsystem.constants.Attributes;
import com.dev.paymentsystem.constants.Parameters;
import com.dev.paymentsystem.constants.Paths;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.ForwardAction;
import com.dev.paymentsystem.controller.action.RedirectAction;
import com.dev.paymentsystem.dao.account.AccountDao;
import com.dev.paymentsystem.dao.account.AccountDaoImpl;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.validator.ErrorHolder;
import com.dev.paymentsystem.validator.AccountValidator;

import javax.servlet.http.HttpServletRequest;

public class DeleteAccountCommand implements Command {

    /**
     * Deletes account
     *
     * @param request HttpServletRequest instance
     * @return new RedirectAction in case of no errors, new ForwardAction otherwise
     * @throws CommandException
     */
    @Override
    public Action execute(HttpServletRequest request) throws CommandException {

        int accountId = Integer.parseInt(request.getParameter(Parameters.ACCOUNT_ID));
        AccountDao accountDao = new AccountDaoImpl();
        ErrorHolder holder = null;
        String path = null;
        Action action = null;
        try {
            holder = AccountValidator.validateDeletionPossibility(accountId);
            if (!holder.haveErrors()) {
                accountDao.deleteAccount(accountId);
                path = Paths.LIST_ACCOUNTS;
                action = new RedirectAction(path);
            } else {
                request.setAttribute(Attributes.ERROR_HOLDER, holder);
                path = String.format(Paths.LIST_CARDS, accountId);
                action = new ForwardAction(path);
            }
        } catch (DaoException e) {
            throw new CommandException("Error by deletion account", e);
        }

        return action;
    }
}