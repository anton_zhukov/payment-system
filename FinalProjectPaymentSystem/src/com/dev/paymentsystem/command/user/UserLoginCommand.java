package com.dev.paymentsystem.command.user;

import com.dev.paymentsystem.command.Command;
import com.dev.paymentsystem.command.exception.CommandException;
import com.dev.paymentsystem.constants.Attributes;
import com.dev.paymentsystem.constants.Pages;
import com.dev.paymentsystem.constants.Parameters;
import com.dev.paymentsystem.constants.Paths;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.ForwardAction;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.dao.user.UserDao;
import com.dev.paymentsystem.dao.user.UserDaoImpl;
import com.dev.paymentsystem.validator.ErrorHolder;
import com.dev.paymentsystem.entity.User;
import com.dev.paymentsystem.util.MD5;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class UserLoginCommand implements Command {

    private static final String WRONG_LOGIN_OR_PASSWORD = "login.error.wrong-login-or-password";

    /**
     * Logs user in
     *
     * @param request HttpServletRequest instance
     * @return new ForwardAction instance
     * @throws com.dev.paymentsystem.command.exception.CommandException
     */
    @Override
    public Action execute(HttpServletRequest request) throws CommandException {

        String login = request.getParameter(Parameters.LOGIN);
        String password = request.getParameter(Parameters.PASSWORD);
        UserDao userDao = new UserDaoImpl();

        String path = Pages.LOGIN_PAGE;
        User user = null;
        try {
            user = userDao.fetchByLoginPassword(login, MD5.hidePass(password));
            if (user != null) {
                HttpSession session = request.getSession();
                session.setAttribute(Attributes.USER, user);
            } else {
                ErrorHolder holder = new ErrorHolder();
                holder.addError(WRONG_LOGIN_OR_PASSWORD);
                request.setAttribute(Attributes.LOGIN, login);
                request.setAttribute(Attributes.ERROR_HOLDER, holder);
            }
        } catch (DaoException e) {
            throw new CommandException("Error by user logging in", e);
        }
        return new ForwardAction(path);
    }
}