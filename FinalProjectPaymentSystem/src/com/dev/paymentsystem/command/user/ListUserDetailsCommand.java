package com.dev.paymentsystem.command.user;

import com.dev.paymentsystem.command.Command;
import com.dev.paymentsystem.command.exception.CommandException;
import com.dev.paymentsystem.constants.Attributes;
import com.dev.paymentsystem.constants.Pages;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.ForwardAction;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.dao.user.UserDetailsDao;
import com.dev.paymentsystem.dao.user.UserDetailsDaoImpl;
import com.dev.paymentsystem.entity.UserDetails;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ListUserDetailsCommand implements Command {

    /**
     * Lists users details on page
     *
     * @param request HttpServletRequest instance
     * @return new ForwardAction instance
     * @throws com.dev.paymentsystem.command.exception.CommandException
     */
    @Override
    public Action execute(HttpServletRequest request) throws CommandException {

        UserDetailsDao userDetailsDao = new UserDetailsDaoImpl();
        List<UserDetails> users = null;
        try {
            users = userDetailsDao.listUserDetails();
            request.setAttribute(Attributes.USERS, users);
        } catch (DaoException e) {
            throw new CommandException("Error by getting list of users", e);
        }

        return new ForwardAction(Pages.USERS_PAGE);
    }
}