package com.dev.paymentsystem.command.user;

import com.dev.paymentsystem.command.Command;
import com.dev.paymentsystem.constants.Attributes;
import com.dev.paymentsystem.constants.Pages;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.ForwardAction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class UserLogoutCommand implements Command {

    /**
     * Logs user out
     *
     * @param request HttpServletRequest instance
     * @return new ForwardAction instance
     */
    @Override
    public Action execute(HttpServletRequest request) {

        HttpSession session = request.getSession();
        session.removeAttribute(Attributes.USER);

        return new ForwardAction(Pages.LOGIN_PAGE);
    }
}