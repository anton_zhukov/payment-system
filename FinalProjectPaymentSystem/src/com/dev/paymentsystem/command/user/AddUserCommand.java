package com.dev.paymentsystem.command.user;

import com.dev.paymentsystem.command.Command;
import com.dev.paymentsystem.command.exception.CommandException;
import com.dev.paymentsystem.constants.Attributes;
import com.dev.paymentsystem.constants.Parameters;
import com.dev.paymentsystem.constants.Paths;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.ForwardAction;
import com.dev.paymentsystem.controller.action.RedirectAction;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.dao.user.UserDao;
import com.dev.paymentsystem.dao.user.UserDaoImpl;
import com.dev.paymentsystem.entity.User;
import com.dev.paymentsystem.util.MD5;
import com.dev.paymentsystem.validator.ErrorHolder;
import com.dev.paymentsystem.validator.UserValidator;

import javax.servlet.http.HttpServletRequest;

public class AddUserCommand implements Command {

    /**
     * Adds new user
     *
     * @param request HttpServletRequest instance
     * @return new RedirectAction in case of no errors, new ForwardAction otherwise
     * @throws com.dev.paymentsystem.command.exception.CommandException
     */
    @Override
    public Action execute(HttpServletRequest request) throws CommandException {

        String tempPass = request.getParameter(Parameters.PASSWORD);
        String repeatPassword = request.getParameter(Parameters.REPEAT_PASSWORD);
        User user = parse(request);

        UserDao userDao = new UserDaoImpl();

        ErrorHolder holder = null;
        Action action = null;
        try {
            holder = UserValidator.validateUser(user, repeatPassword, tempPass);
            if (!holder.haveErrors()) {
                user.setPassword(MD5.hidePass(tempPass));
                userDao.addUser(user);
                action = new RedirectAction(Paths.VIEW_POST_REGISTRATION);
            } else {
                request.setAttribute(Attributes.ERROR_HOLDER, holder);
                request.setAttribute(Attributes.USER, user);
                action = new ForwardAction(Paths.VIEW_REGISTRATION);
            }
        } catch (DaoException e) {
            throw new CommandException("Error by adding user", e);
        }

        return action;
    }

    /**
     * Parses data for getting User instance
     *
     * @param request HttpServletRequest instance
     * @return new User instance
     */
    private User parse(HttpServletRequest request) {

        String login = request.getParameter(Parameters.LOGIN);
        String name = request.getParameter(Parameters.NAME);
        String surname = request.getParameter(Parameters.SURNAME);
        String email = request.getParameter(Parameters.EMAIL);

        User user = new User();
        user.setLogin(login);
        user.setName(name);
        user.setSurname(surname);
        user.setEmailAddress(email);

        return user;
    }
}