package com.dev.paymentsystem.command.bank;

import com.dev.paymentsystem.command.Command;
import com.dev.paymentsystem.command.exception.CommandException;
import com.dev.paymentsystem.constants.Parameters;
import com.dev.paymentsystem.constants.Paths;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.RedirectAction;
import com.dev.paymentsystem.dao.bank.BankDao;
import com.dev.paymentsystem.dao.bank.BankDaoImpl;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.Bank;

import javax.servlet.http.HttpServletRequest;

public class AddBankCommand implements Command {

    /**
     * Adds new bank
     *
     * @param request HttpServletRequest instance
     * @return new RedirectAction instance
     * @throws com.dev.paymentsystem.command.exception.CommandException
     */
    @Override
    public Action execute(HttpServletRequest request) throws CommandException {

        Bank bank = parse(request);

        BankDao bankDao = new BankDaoImpl();
        try {
            bankDao.addBank(bank);
        } catch (DaoException e) {
            throw new CommandException("Error by adding bank", e);
        }
        return new RedirectAction(Paths.LIST_BANKS);
    }

    /**
     * Parses data for getting Bank instance
     *
     * @param request HttpServletRequest instance
     * @return new Bank instance
     */
    private Bank parse(HttpServletRequest request) {
        Bank bank = new Bank();
        bank.setBankName(request.getParameter(Parameters.BANK_NAME));
        return bank;
    }
}