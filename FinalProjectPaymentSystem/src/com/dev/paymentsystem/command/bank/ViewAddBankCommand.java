package com.dev.paymentsystem.command.bank;

import com.dev.paymentsystem.command.Command;
import com.dev.paymentsystem.constants.Pages;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.ForwardAction;

import javax.servlet.http.HttpServletRequest;

public class ViewAddBankCommand implements Command {

    /**
     * Forwards to add bank page
     *
     * @param request HttpServletRequest instance
     * @return new ForwardAction instance
     */
    @Override
    public Action execute(HttpServletRequest request) {

        return new ForwardAction(Pages.ADD_BANK_PAGE);
    }
}