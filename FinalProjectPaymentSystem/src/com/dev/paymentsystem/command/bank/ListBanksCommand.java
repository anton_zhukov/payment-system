package com.dev.paymentsystem.command.bank;

import com.dev.paymentsystem.command.Command;
import com.dev.paymentsystem.command.exception.CommandException;
import com.dev.paymentsystem.constants.Attributes;
import com.dev.paymentsystem.constants.Pages;
import com.dev.paymentsystem.controller.action.Action;
import com.dev.paymentsystem.controller.action.ForwardAction;
import com.dev.paymentsystem.dao.bank.BankDao;
import com.dev.paymentsystem.dao.bank.BankDaoImpl;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.Bank;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ListBanksCommand implements Command {

    /**
     * Lists banks on page
     *
     * @param request HttpServletRequest instance
     * @return new ForwardAction instance
     * @throws com.dev.paymentsystem.command.exception.CommandException
     */
    @Override
    public Action execute(HttpServletRequest request) throws CommandException {

        BankDao bankDao = new BankDaoImpl();
        List<Bank> banks = null;
        try {
            banks = bankDao.listBanks();
            request.setAttribute(Attributes.BANKS, banks);
        } catch (DaoException e) {
            throw new CommandException("Error by getting list of banks", e);
        }

        return new ForwardAction(Pages.BANKS_PAGE);
    }
}