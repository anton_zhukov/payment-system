package com.dev.paymentsystem.validator;

import com.dev.paymentsystem.dao.account.AccountDao;
import com.dev.paymentsystem.dao.account.AccountDaoImpl;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.Payment;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validator for actions with payments
 */
public class PaymentValidator {

    private static final Pattern RECEIVER_ACCOUNT_NUMBER_PATTERN = Pattern.compile("[A-Z]{1}\\d{6}");

    private static final String NOT_ENOUGH_MONEY = "add-payment.error.not-enough-money";
    private static final String WRONG_RECEIVER_ACCOUNT = "add-payment.error.wrong-receiver-account";

    /**
     * Validates possibility of adding payment
     *
     * @param payment   payment for validation
     * @param accountId parameter for validation process
     * @return instance, that contains localization keys for error messages and additional values
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    public static ErrorHolder validatePayment(Payment payment, int accountId) throws DaoException {

        ErrorHolder holder = new ErrorHolder();
        if (!validateEnoughMoneyForPayment(payment.getAmount(), accountId)) {
            holder.addError(NOT_ENOUGH_MONEY);
        }
        if (!validateReceiverAccountNumber(payment.getReceiverAccount())) {
            holder.addError(WRONG_RECEIVER_ACCOUNT);
        }
        return holder;
    }

    /**
     * Validates whether it's enough money for payment
     *
     * @param paymentAmount amount of payment
     * @param accountId     parameter for checking current balance
     * @return {@code true} if account has enough money for payment; {@code false} otherwise
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    private static boolean validateEnoughMoneyForPayment(double paymentAmount, int accountId) throws DaoException {
        AccountDao accountDao = new AccountDaoImpl();
        return accountDao.fetchCurrentBalance(accountId) > paymentAmount;
    }

    /**
     * Validates whether receiver account number format is valid
     *
     * @param receiverAccount receiver account number by payment
     * @return {@code true} if receiver account number format is valid; {@code false} otherwise
     */
    private static boolean validateReceiverAccountNumber(String receiverAccount) {
        boolean isCorrect = false;
        Matcher m = RECEIVER_ACCOUNT_NUMBER_PATTERN.matcher(receiverAccount);
        if (m.matches()) {
            isCorrect = true;
        }
        return isCorrect;
    }
}