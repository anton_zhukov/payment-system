package com.dev.paymentsystem.validator;

import com.dev.paymentsystem.dao.account.AccountDao;
import com.dev.paymentsystem.dao.account.AccountDaoImpl;
import com.dev.paymentsystem.dao.currency.CurrencyDao;
import com.dev.paymentsystem.dao.currency.CurrencyDaoImpl;
import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.Account;

/**
 * Validator for actions with accounts
 */
public class AccountValidator {

    private static final byte MIN_RANGE = 0;
    private static final double MAX_RANGE = 1000000000000.0;

    private static final String WRONG_MIN_INIT_BALANCE = "add-account.error.wrong-min-init-balance";
    private static final String WRONG_MAX_INIT_BALANCE = "add-account.error.wrong-max-init-balance";
    private static final String ACCOUNT_DELETION_ERROR = "cards.error.account-has-cards";
    private static final String WRONG_MIN_REPLENISH_AMOUNT = "replenish-balance.error.amount-min-range";
    private static final String WRONG_MAX_REPLENISH_AMOUNT = "replenish-balance.error.amount-max-range";

    /**
     * Validates account adding
     *
     * @param account instance for validation
     * @return instance, that contains localization keys for error messages and additional values
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    public static ErrorHolder validateAccount(Account account) throws DaoException {

        ErrorHolder holder = new ErrorHolder();
        CurrencyDao currencyDao = new CurrencyDaoImpl();
        double minInitBalanceRange = currencyDao.fetchMinAmount(account.getCurrencyId());
        if (account.getCurrentBalance() < minInitBalanceRange) {
            holder.addError(WRONG_MIN_INIT_BALANCE, String.valueOf(minInitBalanceRange));
        }
        if (account.getCurrentBalance() > MAX_RANGE) {
            holder.addError(WRONG_MAX_INIT_BALANCE);
        }
        return holder;
    }

    /**
     * Validates possibility of account deletion
     *
     * @param accountId id of account for deletion
     * @return instance, that contains localization keys for error messages and additional values
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    public static ErrorHolder validateDeletionPossibility(int accountId) throws DaoException {

        AccountDao accountDao = new AccountDaoImpl();
        ErrorHolder holder = new ErrorHolder();
        if (accountDao.doesAccountHaveCards(accountId)) {
            holder.addError(ACCOUNT_DELETION_ERROR);
        }
        return holder;
    }

    /**
     * Validates amount of replenishment
     *
     * @param replenishAmount amount of balance replenishment
     * @return instance, that contains localization keys for error messages and additional values
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    public static ErrorHolder validateReplenishAmount(double replenishAmount) throws DaoException {

        ErrorHolder holder = new ErrorHolder();
        if (replenishAmount < MIN_RANGE) {
            holder.addError(WRONG_MIN_REPLENISH_AMOUNT);
        } else if (replenishAmount > MAX_RANGE) {
            holder.addError(WRONG_MAX_REPLENISH_AMOUNT);
        }
        return holder;
    }
}