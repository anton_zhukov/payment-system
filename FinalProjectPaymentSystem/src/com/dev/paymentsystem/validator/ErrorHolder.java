package com.dev.paymentsystem.validator;

import com.dev.paymentsystem.entity.ErrorMessage;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ErrorHolder {

    private List<ErrorMessage> errors;

    public ErrorHolder() {
        errors = new LinkedList<>();
    }

    public boolean haveErrors() {
        return errors.size() > 0;
    }

    public List<ErrorMessage> getErrors() {
        return errors;
    }

    public void addError(String error) {
        ErrorMessage message = new ErrorMessage(error);
        errors.add(message);
    }

    public void addError(String error, String value) {
        List<String> values = new LinkedList<>();
        values.add(value);
        addError(error, values);
    }

    public void addError(String error, List<String> values) {
        ErrorMessage message = new ErrorMessage(error, values);
        errors.add(message);
    }
}