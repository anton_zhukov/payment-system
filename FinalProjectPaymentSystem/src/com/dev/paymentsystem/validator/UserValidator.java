package com.dev.paymentsystem.validator;

import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.dao.user.UserDao;
import com.dev.paymentsystem.dao.user.UserDaoImpl;
import com.dev.paymentsystem.entity.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validator for actions with users
 */
public class UserValidator {

    private static final Pattern EMAIL_PATTERN = Pattern.compile("\\w+([\\.-]*\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,4})+");
    private static final Pattern NAME_AND_SURNAME_PATTERN = Pattern.compile("[A-ZА-Я][a-zа-я]+");

    private static final String PASSWORDS_NOT_EVEN = "registration.error.passwords-not-even";
    private static final String WRONG_EMAIL_FORMAT = "registration.error.wrong-email-format";
    private static final String WRONG_PASSWORD_LENGTH = "registration.error.wrong-password-length";
    private static final String WRONG_NAME_FORMAT = "registration.error.wrong-name-format";
    private static final String WRONG_SURNAME_FORMAT = "registration.error.wrong-surname-format";
    private static final String LOGIN_IS_BUSY = "registration.error.login-is-busy";

    /**
     * Validates possibility of adding user
     *
     * @param user           user for validation
     * @param repeatPassword additional parameter for validation
     * @param tempPass       additional parameter for validation
     * @return instance, that contains localization keys for error messages and additional values
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    public static ErrorHolder validateUser(User user, String repeatPassword, String tempPass) throws DaoException {

        UserDao userDao = new UserDaoImpl();
        ErrorHolder holder = new ErrorHolder();

        if (!validateInputPassword(tempPass, repeatPassword)) {
            holder.addError(PASSWORDS_NOT_EVEN);
        }
        if (!validateEmail(user.getEmailAddress())) {
            holder.addError(WRONG_EMAIL_FORMAT);
        }
        if (validatePasswordLength(tempPass)) {
            holder.addError(WRONG_PASSWORD_LENGTH);
        }
        if (!validateNameOrSurname(user.getName())) {
            holder.addError(WRONG_NAME_FORMAT);
        }
        if (!validateNameOrSurname(user.getSurname())) {
            holder.addError(WRONG_SURNAME_FORMAT);
        }
        if (userDao.fetchByLogin(user.getLogin())) {
            holder.addError(LOGIN_IS_BUSY);
        }
        return holder;
    }

    /**
     * Validates whether password and repeat password match
     *
     * @param password       password for validation
     * @param repeatPassword repeat password for validation
     * @return {@code true} if passwords match; {@code false} otherwise
     */
    private static boolean validateInputPassword(String password, String repeatPassword) {
        return password.equals(repeatPassword);
    }

    /**
     * Validates whether password length is valid
     *
     * @param password password for validation
     * @return {@code true} if password length is valid; {@code false} otherwise
     */
    private static boolean validatePasswordLength(String password) {
        return password.length() < 6 || password.length() >= 20;
    }

    /**
     * Validates whether email format is valid
     *
     * @param email email for validation
     * @return {@code true} if email format is valid; {@code false} otherwise
     */
    private static boolean validateEmail(String email) {
        boolean isCorrect = false;
        Matcher m = EMAIL_PATTERN.matcher(email);
        if (m.matches()) {
            isCorrect = true;
        }
        return isCorrect;
    }

    /**
     * Validate whether name format is valid
     *
     * @param option Name or Surname for validation
     * @return {@code true} if name format is valid; {@code false} otherwise
     */
    private static boolean validateNameOrSurname(String option) {
        boolean isCorrect = false;
        Matcher m = NAME_AND_SURNAME_PATTERN.matcher(option);
        if (m.matches()) {
            isCorrect = true;
        }
        return isCorrect;
    }
}