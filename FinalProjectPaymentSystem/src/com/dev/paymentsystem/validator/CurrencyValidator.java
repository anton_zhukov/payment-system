package com.dev.paymentsystem.validator;

import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.Currency;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CurrencyValidator {

    private static final byte MIN_RANGE = 0;
    private static final double MAX_RANGE = 1000000000000.0;
    private static final Pattern CURRENCY_CODE_PATTERN = Pattern.compile("[A-Z]{3}");
    private static final String WRONG_MIN_AMOUNT = "add-currency.error.wrong-amount-min-range";
    private static final String WRONG_MAX_AMOUNT = "add-currency.error.wrong-amount-max-range";
    private static final String WRONG_CURRENCY_CODE = "add-currency.error.wrong-currency-code-format";

    /**
     * Validates min initial amount on balance range by adding new currency
     *
     * @param currency Validating currency
     * @return instance, that contains localization keys for error messages and additional values
     * @throws com.dev.paymentsystem.dao.exception.DaoException
     */
    public static ErrorHolder validateCurrency(Currency currency) throws DaoException {

        ErrorHolder holder = new ErrorHolder();
        if (currency.getMinAmount() < MIN_RANGE) {
            holder.addError(WRONG_MIN_AMOUNT);
        } else if (currency.getMinAmount() > MAX_RANGE) {
            holder.addError(WRONG_MAX_AMOUNT);
        }
        if (!validateCurrencyCode(currency.getCurrencyCode())) {
            holder.addError(WRONG_CURRENCY_CODE);
        }
        return holder;
    }

    /**
     * Validates whether currency code format is valid
     *
     * @param currencyCode currency code
     * @return {@code true} if currencyCode format is valid; {@code false} otherwise
     */
    private static boolean validateCurrencyCode(String currencyCode) {
        boolean isCorrect = false;
        Matcher m = CURRENCY_CODE_PATTERN.matcher(currencyCode);
        if (m.matches()) {
            isCorrect = true;
        }
        return isCorrect;
    }
}