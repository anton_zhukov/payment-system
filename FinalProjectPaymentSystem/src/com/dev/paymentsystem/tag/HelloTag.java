package com.dev.paymentsystem.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Tag for saying "Hello, user" in header.jsp
 */
public class HelloTag extends TagSupport {

    private String hello;
    private String name;
    private String surname;

    public void setHello(String hello) {
        this.hello = hello;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            pageContext.getOut().write(hello + ", " + name + " " + surname);
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }
}