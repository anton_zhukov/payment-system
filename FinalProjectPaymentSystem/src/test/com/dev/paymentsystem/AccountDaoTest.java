package test.com.dev.paymentsystem;

import com.dev.paymentsystem.dao.account.AccountDao;
import com.dev.paymentsystem.dao.account.AccountDaoImpl;
import com.dev.paymentsystem.dao.exception.DaoException;
import org.junit.Assert;
import org.junit.Test;

public class AccountDaoTest {

    @Test
    public void doesAccountHaveCardsTest() {

        int accountId = 2;
        AccountDao dao = new AccountDaoImpl();
        boolean actual = false;
        try {
            actual = dao.doesAccountHaveCards(accountId);
        } catch (DaoException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(actual);
    }
}