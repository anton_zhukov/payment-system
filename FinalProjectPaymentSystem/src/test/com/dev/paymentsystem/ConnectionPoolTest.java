package test.com.dev.paymentsystem;

import com.dev.paymentsystem.pool.configuration.ConnectionPoolConfig;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionPoolTest {

    @Test
    public void testGetConnection() throws SQLException {

        ConnectionPoolConfig config = ConnectionPoolConfig.getInstance();
        final String URL = config.getString("db.url");
        final String USER_NAME = config.getString("db.username");
        final String PASSWORD = config.getString("db.password");

        DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        Connection connection = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
        Assert.assertNotNull(connection);
    }
}
