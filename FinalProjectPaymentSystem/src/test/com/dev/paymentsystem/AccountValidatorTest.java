package test.com.dev.paymentsystem;

import com.dev.paymentsystem.dao.exception.DaoException;
import com.dev.paymentsystem.entity.Account;
import com.dev.paymentsystem.validator.AccountValidator;
import com.dev.paymentsystem.validator.ErrorHolder;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AccountValidatorTest {

    private Account account;

    @Before
    public void initAccount() {
        account = new Account();
    }

    @After
    public void clearAccount() {
        account = null;
    }

    @Test
    public void takeHolderTest() {

        account.setAccountId(1);
        account.setCurrentBalance(1);
        account.setBankId(1);
        account.setCurrencyId(1);
        account.setStatusId(1);
        account.setAccountNumber("R111111");
        account.setUserId(200);
        ErrorHolder holder = null;
        try {
            holder = AccountValidator.validateAccount(account);
        } catch (DaoException e) {
            e.printStackTrace();
        }

        Assert.assertNotNull(holder);
    }
}